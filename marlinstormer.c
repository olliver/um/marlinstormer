#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <limits.h>
#include <linux/serial.h>
#include <poll.h>
#include <errno.h>
#include <stropts.h>
#include <asm/ioctls.h>
#include <libgen.h>
#include <sys/timerfd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/eventfd.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <setjmp.h>
#include <execinfo.h>
#include <stddef.h>

#include "termios_conv.h"
#include "hexdump.h"
#include "transport.h"
#include "listener.h"
#include "server.h"
#include "packetizer.h"


/*****************************************************************************/
/*** Macros								   ***/
/*****************************************************************************/
/* Generic properties */
#define DEFAULT_TTY		"/dev/ttyS2"
#define DEFAULT_BITRATE		250000
#define MAX_GCODE_LEN		100
#define MAX_CMDS		10
#define MAX_REPLY_DATA		1023

/* Macro to divide integers with proper round-off (beware of over-/underflows!) */
#define INTDIV(n, d)		((n)+((((n)>=0&&(d)>=0)||((n)<0&&(d)<0))?((d)/2):-((d)/2)))/(d)

/* Macro to calculate the number of members in an array */
#define ARRAY_SIZE(arr)		(sizeof(arr) / sizeof((arr)[0]))

/* TCP listener port to receive new clients on */
#define SERVER_TCP_PORT		1979

//#define DEBUG

/*****************************************************************************/
/*** Types								   ***/
/*****************************************************************************/
struct command_entry_t
{
	uint32_t		refnr;                    /* Reference number the client gave this command */
	unsigned char		is_motion;                /* Flag indicating whether or not this is a motion command */
	size_t			gcode_len;                /* Command g-code length */
	unsigned char		gcode[MAX_GCODE_LEN];     /* Command g-code */
};

struct command_queue_t
{
	unsigned int		head;                     /* Index to the entry next up for transmission */
	unsigned int		tail;                     /* Index to the entry available for a new command */
	int			read_fd;                  /* Semaphore protecting reading */
	int			write_fd;                 /* Semaphore protecting writing */
	struct command_entry_t	entry[MAX_CMDS + 1];      /* Queue entries */
};

struct rplq_t
{
	unsigned int		head;                     /* Index to the entry next up for transmission */
	unsigned int		tail;                     /* Index to the entry available for a new command */
	int			read_fd;                  /* Semaphore protecting reading */
//	int			write_fd;                 /* Semaphore protecting writing */
	unsigned char		data[MAX_REPLY_DATA + 1]; /* Queue data */
};

struct session_t
{
	unsigned char		in_use;                   /* Flag indicated whether or not this session is in use */
	void			*server;
	void			*packetizer;
	unsigned char		paused;                   /* Flag indicating outputting Marlin motion commands from the client should be paused TODO: Implement */
	unsigned char		mute_marlin_boardcasts;   /* Flag indicating whether Marlin broadcasts should be parsed on to the client, or should be muted TODO: Implement */
	unsigned int		perf_counter;             /* Performance counter, keeping track of the number of commands per second received */
	/* Command queue data */
	struct command_queue_t	command_queue;            /* Command queue, holding a complete Marlin command in each entry */
};

struct envelope_t
{
	unsigned char	type;
	unsigned char	packet[UCHAR_MAX - sizeof(unsigned char /* type */)];
} __attribute__((packed));

struct packet_motion_command_t {
	unsigned char	data[sizeof(((struct envelope_t *)0)->packet)];
} __attribute__((packed));

struct packet_command_t {
	uint32_t	refnr;
	unsigned char	data[sizeof(((struct envelope_t *)0)->packet) - sizeof(uint32_t /* refnr */)];
} __attribute__((packed));

struct packet_reply_t {
	uint32_t	refnr;
	unsigned char	data[sizeof(((struct envelope_t *)0)->packet) - sizeof(uint32_t /* refnr */)];
} __attribute__((packed));

struct packet_boardcast_t {
	unsigned char	data[sizeof(((struct envelope_t *)0)->packet)];
} __attribute__((packed));

struct packet_queue_request_t {
	uint32_t	refnr;
} __attribute__((packed));

struct packet_queue_reply_t {
	uint32_t	refnr;
	unsigned char	inflight_size;
	unsigned char	inflight_used;
	unsigned char	motion_size;
	unsigned char	motion_used;
} __attribute__((packed));


/*****************************************************************************/
/*** Globals								   ***/
/*****************************************************************************/
/* Command line options */
static char			*ttyport;                 /* Serial port device node name */
static speed_t			bitrate;                  /* Serial port bit rate in b/s */
static int			flowcontrol;              /* Flag indicating if hardware flow control is to be used */
static int			verbosity;                /* Verbosity level */

/* Back-trace data */
static void			*bt_trace[20];            /* Back-trace buffer */
static size_t			bt_count;                 /* Back-trace count */
static jmp_buf			bt_env;                   /* Back-trace environment */

/* Serial port properties */
static int			tty_fd;                   /* File descriptor */
static struct serial_struct	tty_oldserinfo;           /* Backup: Restore on exit */
static struct termios		tty_oldtio;               /* Backup: Restore on exit */

/* Marlin data transport data */
static void			*transport;               /* Marlin data transport instance */
static int			transp_fd;
static int			transp_motionq_fd;
static int			transp_inflightq_fd;

/* Client session data */
static struct session_t		sessions[8];


/* Reply queue data */
static struct rplq_t		rplq;                     /* Reply queue, holding a stream of bytes containing a number of replies */

static void			*listener;

static int			perf_timer_fd;


/*****************************************************************************/
/*** Helper statics							   ***/
/*****************************************************************************/
#ifdef DEBUG
static unsigned long timestamp_ms(void)
{
	struct timespec		timestamp;
	unsigned long		timestamp_us;

	clock_gettime(CLOCK_MONOTONIC, &timestamp);

	timestamp_us = (timestamp.tv_sec)  * 1000000 +
	               (timestamp.tv_nsec) / 1000;

	return timestamp_us / 1000;
}
#endif /* DEBUG */


#ifndef __x86_64__
static void reset_marlin(unsigned char hold)
{
	int			file;

	if ((file = open("/dev/gpio/avr_reset", O_WRONLY)) == -1) {
		perror("Error opening Marlin reset node");
		return;
	}

	if (write(file, "0", 1) == -1) {
		perror("Error putting Marlin in reset");
		goto err_write;
	}

	if (!hold) {
		sleep(1);
		if (write(file, "1", 1) == -1) {
			perror("Error taking Marlin out of reset");
			goto err_write;
		}
		sleep(3);
	}
err_write:
	close(file);
}
#endif /* !__x86_64__ */


/*****************************************************************************/
/*** Marlin handler statics						   ***/
/*****************************************************************************/
/*
 * This function is called when out-of-band data from Marlin is received.
 * Instead of calling this function for each byte received,  out-of-band
 * bytes are buffered and parsed with this function when an (in-band)
 * packet is received. This function runs in the context from which
 * transport_process is called.
 * data		Is the pointer to a block of out-of-band data (not zero-terminated)
 * length	Is the out-of-band data block length
 */
static void handle_marlin_oob(void *data, unsigned char len)
{
#ifdef DEBUG
	fprintf(stderr, "Received OOB:\n");
	hexdump(stderr, data, len);
#endif /* DEBUG */
}


/*
 * This function is called when a reply from Marlin in a TYPE__REPLY
 * packet is received. It runs in the context from which transport_process
 * is called.
 * user_context Is the pointer given when sending the command this reply replies to
 * user_refnr	Is the refnr given when sending the command this reply replies to
 * data		Is the pointer to the full reply (not necessarily a string, but always zero-terminated)
 * length	Is the reply length
 */
void handle_marlin_reply(void *user_context, uint32_t user_refnr, char *data, size_t length)
{
	struct session_t	*session = (struct session_t *)user_context;
	struct envelope_t	envelope = {
		.type = 'R'
	};
	struct packet_reply_t	*packet_reply = (struct packet_reply_t *)&envelope.packet;

	/* Memcpy: Destination may be unaligned */
	memcpy(&packet_reply->refnr, &user_refnr, sizeof(packet_reply->refnr));
	memcpy(packet_reply->data, data, length);

	packetizer_send(session->packetizer, &envelope, sizeof(envelope.type) + sizeof(packet_reply->refnr) + length);
}


/*
 * This function is called when a slave-initiated message from Marlin in a
 * TYPE__BROADCAST packet is received. It runs in the context from which
 * transport_process is called.
 * data		Is the pointer to the full reply (not necessarily a string, but always zero-terminated)
 * length	Is the reply length
 */
void handle_marlin_broadcast(char *data, size_t length)
{
	unsigned int		session_ndx;
	struct envelope_t	envelope = {
		.type = 'B'
	};
	struct packet_boardcast_t *packet_boardcast = (struct packet_boardcast_t *)&envelope.packet;

	fprintf(stderr, "Received marlin broadcast ('%.*s')\n", length, (char *)data);

	memcpy(packet_boardcast->data, data, length);

	for (session_ndx = 0; session_ndx < ARRAY_SIZE(sessions); session_ndx++)
		if (sessions[session_ndx].in_use)
			packetizer_send(sessions[session_ndx].packetizer, &envelope, sizeof(envelope.type) + length);
}


/*
 * This function provides raw Marlin write functionality to the protocol stack.
 * It is called whenever the protocol stack needs to send data to Marlin. It
 * runs in the context from which transport_process is called.
 * data		Is the pointer to data that needs to be sent
 * length	Is the data length
 */
static ssize_t write_marlin(void *data, ssize_t length)
{
	ssize_t			bytes;

	if ((bytes = write(tty_fd, data, length)) == -1)
		perror("Error writing Marlin data");

	return bytes;
}


/*****************************************************************************/
/*** Queue related statics						   ***/
/*****************************************************************************/
/*
 * This function is helper function for both handle_marlin_command and
 * handle_marlin_motion_command and it puts a Marlin command into the transport
 * layer. This function runs in a server prthread context.
 * command_queue	Is a pointer to the command queue to be used
 * is_motion		Is a flag indicating whether of not this is a motion command
 * refnr		Is the number to refer to when sending back replies
 * gcode		Is a pointer to the full g-code command (not zero-terminated)
 * gcode_len		Is the command length
 */
static void queue_marlin_command(struct command_queue_t *command_queue, unsigned char is_motion, unsigned int refnr, void *gcode, unsigned char gcode_len)
{
	struct command_entry_t	*entry = &command_queue->entry[command_queue->head];
	eventfd_t		dummy;

	/* Take the command queue write semaphore */
	if (eventfd_read(command_queue->write_fd, &dummy) == -1) {
		perror("Error reading command queue read eventfd");
		exit(1);
	}

	/* Copy the command received into the head command queue entry */
	entry->refnr     = refnr;
	entry->is_motion = is_motion;
	entry->gcode_len = gcode_len;
	memcpy(entry->gcode, gcode, gcode_len);

#ifdef DEBUG
	fprintf(stderr, "%ld: Queuing command '%.*s'\n", timestamp_ms(), gcode_len, (char *)entry->data);
#endif /* DEBUG */

	/* Add the command to the command queue */
	if (++command_queue->head > MAX_CMDS)
		command_queue->head = 0;
	/* Release the command queue read semaphore, enabling a new read from the command queue */
	if (eventfd_write(command_queue->read_fd, 1) == -1) {
		perror("Error writing command queue write eventfd");
		exit(1);
	}
}


/*****************************************************************************/
/*** Client command handler statics					   ***/
/*****************************************************************************/
/*
 * This function is called when a Marlin non-motion command is received from the
 * client. This function runs in a server prthread context.
 * session	Is a pointer to the session producing the command
 * data		Is a pointer to the full command (not zero-terminated)
 * length	Is the command length
 */
static void handle_marlin_command(struct session_t *session, void *data, unsigned char length)
{
	uint32_t		refnr = 0;
	struct packet_command_t	*packet_command = (struct packet_command_t *)data;

	/* Memcpy: Source may be unaligned */
	memcpy(&refnr, &packet_command->refnr, sizeof(refnr));

	fprintf(stderr, "Received marlin non-motion-command from session @%p: refnr = 0x%.8x ('%.*s')\n", session, refnr, length - sizeof(packet_command->refnr), (char *)&packet_command->data);
	queue_marlin_command(&session->command_queue, NO_MOTION_CMD, refnr, &packet_command->data, length - sizeof(packet_command->refnr));
}


/*
 * This function is called when a Marlin motion command is received from the
 * client. This function runs in a server prthread context.
 * session	Is a pointer to the session producing the motion command
 * data		Is a pointer to the full command (not zero-terminated)
 * length	Is the command length
 */
static void handle_marlin_motion_command(struct session_t *session, void *data, unsigned char length)
{
	fprintf(stderr, "Received marlin motion-command from session @%p ('%.*s')\n", session, length, (char *)data);
	queue_marlin_command(&session->command_queue, MOTION_CMD, 0, data, length);
}


static void handle_queue_command(struct session_t *session, void *data, unsigned char length)
{
	struct packet_queue_request_t	*queue_command = (struct packet_queue_request_t *)data;
	struct envelope_t	envelope = {
		.type = 'Q'
	};
	struct packet_queue_reply_t	*queue_reply = (struct packet_queue_reply_t *)&envelope.packet;

	fprintf(stderr, "Received Queue status command from session @%p\n", session);

	/* Memcpy: Source and destination may be unaligned */
	memcpy(&queue_reply->refnr, &queue_command->refnr, sizeof(queue_reply->refnr));

	transport_get_queueinfo(transport, &queue_reply->inflight_size, &queue_reply->inflight_used, &queue_reply->motion_size, &queue_reply->motion_used);

	packetizer_send(session->packetizer, &envelope, sizeof(envelope.type) + sizeof(struct packet_queue_reply_t));
}


/*****************************************************************************/
/*** Client related statics						   ***/
/*****************************************************************************/
/*
 * This function is called when a command is received from the client. This
 * function runs in a server prthread context.
 * user_data	Is the pointer given during constructing the packerizer instance (contains session pointer)
 * data		Is a pointer to the full command (not zero-terminated)
 * length	Is the command length
 */
static void handle_client_packet(void *user_data, void *packet, unsigned char packet_len)
{
	/* Command handler table */
	static struct handler_table_t {
		unsigned char		type;
		void			(*handler)(struct session_t *session, void *data, unsigned char len);
	} handler_table[] = {
		{.type = 'C', .handler = handle_marlin_command},
		{.type = 'M', .handler = handle_marlin_motion_command},
		{.type = 'Q', .handler = handle_queue_command}
	};

	struct session_t	*session = (struct session_t *)user_data;
	struct envelope_t	*envelope = (struct envelope_t *)packet;
	int			table_ndx = 0;

	while (table_ndx < ARRAY_SIZE(handler_table)) {
		if (envelope->type == handler_table[table_ndx].type) {
			if (handler_table[table_ndx].handler)
				handler_table[table_ndx].handler(session, envelope->packet, packet_len - offsetof(struct envelope_t, packet));

			/* Increase the perfomance counter */
			session->perf_counter++;
			return;
		}
		table_ndx++;
	}
	fprintf(stderr, "Error handling unsupported %.2x packet (%d bytes)\n", envelope->type, packet_len - sizeof(envelope->type));
}


/*
 * This function provides raw client write functionality to the packetizer. It
 * is called whenever the packetizer needs to send data to the client. It runs
 * in the context from which transport_process is called.
 * user_data	Is the pointer given during constructing the packerizer instance (contains session pointer)
 * data		Is the pointer to data that needs to be sent
 * length	Is the data length
 */
static ssize_t write_client(void *user_data, void *data, ssize_t length)
{
	struct session_t	*session = (struct session_t *)user_data;

	return server_write(session->server, data, length, 0);
}


/*****************************************************************************/
/*** Server related statics						   ***/
/*****************************************************************************/
/*
 * This function is called when raw data is received from a client TCP socket.
 * It runs in a server prthread context.
 * user_data	Is the pointer given during constructing the server instance (contains session pointer)
 * data		Is a pointer to a block of received data
 * length	Is the length of the received data
 */
static void handle_client_data(void *user_data, unsigned char *data, size_t len)
{
	struct session_t	*session = (struct session_t *)user_data;

	packetizer_process(session->packetizer, data, len);
}


/*
 * This function is called after a client has disconnected. It runs in the
 * server pthread context.
 * user_data	Is the pointer given during constructing the server instance (contains session pointer)
 * reason	Is an enum indicating the reason for disconnection
 * During this function the file descriptor is still open, but is no longer
 * useful. It will be closed by the server after this function returns.
 */
void handle_client_disconnected(void *user_data, enum reason_t reason)
{
	struct session_t	*session = (struct session_t *)user_data;
	unsigned int		session_ndx;

	fprintf(stderr, "Session @%p disconnected (reason = %d)\n", session, reason);

	/* Do not delete server: It has already deleted itself TODO: Fix server! */

	packetizer_delete(&session->packetizer);

//	pthread_mutex_lock(&session_mutex);
	for (session_ndx = 0; session_ndx < ARRAY_SIZE(sessions); session_ndx++)
		if (&sessions[session_ndx] == session) {
			sessions[session_ndx].in_use = 0;
			break;
		}
//	pthread_mutex_unlock(&session_mutex);
}


/*
 * This function is called after a client connection was accepted after is has
 * connected to the the listening socket. It runs in the listener pthread
 * context.
 * user_data	Is the pointer given during constructing the listener instance (not used now)
 * client_ip	Is a pointer to a zero-terminated string containing the client IP address
 * client_fd	Is the connection file descriptor
 * Exiting this function with a < 0 return to indicate failure/unreadyness. In
 * that case the client will be disconnected and the file descriptor will be
 * closed by the listener.
 */
int handle_client_connected(void *user_data, char *client_ip, int client_fd)
{
	struct session_t	*session;
	unsigned int		session_ndx = 0;
	int			result = 0;

//	pthread_mutex_lock(&session_mutex);
	do {
		if (!sessions[session_ndx].in_use) {
			sessions[session_ndx].in_use = 1;
			break;
		}
		session_ndx++;
	} while (session_ndx < ARRAY_SIZE(sessions));
//	pthread_mutex_unlock(&session_mutex);

	/* Check if a free session was found */
	if (session_ndx >= ARRAY_SIZE(sessions)) {
		fprintf(stderr, "No free session for %s-%d\n", client_ip, client_fd);
		result = -EBUSY;
		goto err_session;
	}
	session = &sessions[session_ndx];

	/* Create a packetizer for this session */
	if ((result = packetizer_new(&session->packetizer,
	                         session, handle_client_packet, NULL, write_client)) < 0) {
		fprintf(stderr, "Error %d creating packetizer for %s-%d\n", result, client_ip, client_fd);
		goto err_packetizer;
	}


	/* Create a server for this session NOTE: Do this last because tearing this down will cause handle_client_disconnected to be called */
	if ((result = server_new(&session->server,
	                         session, client_ip, client_fd,
	                         handle_client_data, handle_client_disconnected)) < 0) {
		fprintf(stderr, "Error %d creating server for %s-%d\n", result, client_ip, client_fd);
		goto err_server;
	}

	fprintf(stderr, "Session @%p connected to %s-%d\n", session, client_ip, client_fd);
	return 0;
	/* Deleting the server will cause handle_client_disconnected to be called! */
	server_delete(&session->server);
err_server:
	packetizer_delete(&session->packetizer);
err_packetizer:
//	pthread_mutex_lock(&session_mutex);
	sessions[session_ndx].in_use = 0;
//	pthread_mutex_unlock(&session_mutex);
err_session:
	return result;
}


/*****************************************************************************/
/*** Utilitarian statics						   ***/
/*****************************************************************************/
/*
 * This function is a helper function, to make session queue intialisation
 * easier.
 */
static int init_session_queue(struct session_t *session)
{
	struct command_queue_t	*command_queue = &session->command_queue;
	int			result = 0;

	/* Create an eventfd for the number of free command queue entries */
	if ((command_queue->read_fd = eventfd(0, EFD_SEMAPHORE)) == -1) {
		perror("Error in-flight queue free eventfd");
		result = -errno;
		goto err_command_queue_read_eventfd;
	}
	/* Create an eventfd for the number of used command queue entries */
	if ((command_queue->write_fd = eventfd(MAX_CMDS, EFD_SEMAPHORE)) == -1) {
		perror("Error in-flight queue free eventfd");
		result = -errno;
		goto err_command_queue_write_eventfd;
	}

	return 0;
	close(command_queue->write_fd);
err_command_queue_write_eventfd:
	close(command_queue->read_fd);
err_command_queue_read_eventfd:
	return result;
}


/*
 * This function is a helper function, to make session queue termination
 * easier.
 */
static void term_session_queue(struct session_t *session)
{
	struct command_queue_t	*command_queue = &session->command_queue;

	close(command_queue->write_fd);
	close(command_queue->read_fd);
}


/*
 * This function is called once, to initialize all subsystems.
 */
static int init(void)
{
	int			result = 0;
	int			session_ndx;
#ifndef __x86_64__
	int			file;
	int			standard_bitrate;
	struct termios		newtio;
#endif /* !__x86_64__ */
	struct itimerspec	itval;

	if (tty_fd) {
		result = -EINVAL;
		goto err_param;
	}

#ifndef __x86_64__
	reset_marlin(0);

	/* Open the serial port */
	if (verbosity)
		fprintf(stderr, "Opening %s\n", ttyport);
	if ((file = open(ttyport, O_RDWR | O_NOCTTY)) == -1) {
		perror("Error opening serial port");
		result = -errno;
		goto err_file;
	}
	tty_fd = file;

	/* Store current serial configuration */
	if (ioctl(tty_fd, TIOCGSERIAL, &tty_oldserinfo) == -1) {
		perror("Error backing up serial port configuration");
		result = -errno;
		goto err_backup;
	}

	/* Check if we need to set a custom bit rate */
	if (!(standard_bitrate = termios_bitrate(bitrate))) {
		struct serial_struct	newserinfo = tty_oldserinfo;

		/* Set custom bit rate */
		newserinfo.flags = (newserinfo.flags & ~ASYNC_SPD_MASK) | ASYNC_SPD_CUST;
		if ((newserinfo.custom_divisor = INTDIV(newserinfo.baud_base, bitrate)) < 1)
			newserinfo.custom_divisor = 1;
		if (ioctl(tty_fd, TIOCSSERIAL, &newserinfo) == -1) {
			perror("Error setting custom bit rate");
			result = -errno;
			goto err_setbitrate;
		}

		/* Verify custom bit rate */
		if (ioctl(tty_fd, TIOCGSERIAL, &newserinfo) == -1) {
			perror("Error verifying custom bit rate");
			result = -errno;
			goto err_verifybitrate;
		}
		if (newserinfo.custom_divisor * bitrate != newserinfo.baud_base)
			fprintf(stderr, "Actual bit rate is %d / %d = %f\n", newserinfo.baud_base,
			                newserinfo.custom_divisor, (float)newserinfo.baud_base / newserinfo.custom_divisor);

		/* Set standard bit rate to custom bit rate marking value */
		standard_bitrate = B38400;
	}

	/* FTDI: ASYNC_LOW_LATENCY */

	/* Get the current serial port attributes */
	if (tcgetattr(tty_fd, &tty_oldtio) == -1) {
		perror("Error getting serial port attributes");
		result = -errno;
		goto err_getattr;
	}

	/* Fill out the new serial port attributes */
	bzero(&newtio, sizeof(newtio));
	newtio.c_cflag     = standard_bitrate | CS8 | CLOCAL | CREAD | (flowcontrol?CRTSCTS:0) | ASYNC_LOW_LATENCY;
	newtio.c_iflag     = IGNPAR;
	newtio.c_oflag     = 0;
	newtio.c_lflag     = 0; /* Input mode (non-canonical, no echo,...) */
	newtio.c_cc[VTIME] = 0; /* inter-character timer unused */
	newtio.c_cc[VMIN]  = 0; /* No blocking read */

	/* Flush the buffer */
	if (tcflush(tty_fd, TCIFLUSH) == -1) {
		perror("Error flushing serial port input");
		result = -errno;
		goto err_flush;
	}

	/* Set the new attributes */
	if (tcsetattr(tty_fd, TCSANOW, &newtio) == -1) {
		perror("Error setting serial port attributes");
		result = -errno;
		goto err_setattr;
	}
#endif /* !__x86_64__ */

	/* Create a timer to measure performance */
        if ((perf_timer_fd = timerfd_create(CLOCK_MONOTONIC, 0)) < 0) {
		perror("Error creating performance timer");
                result = -errno;
                goto err_perf_timer;
        }
        /* Make performance timer periodic */
        memset(&itval, 0, sizeof(itval));
        itval.it_interval.tv_sec  = 1;
        itval.it_value.tv_sec     = 1;
        if ((result = timerfd_settime(perf_timer_fd, 0, &itval, NULL)) < 0) {
		perror("Error setting performance timer");
                result = -errno;
                goto err_set_perf_timer;
        }

	/* Create a Marlin data transport instance */
	if ((transp_fd = transport_new(&transport, handle_marlin_reply, handle_marlin_broadcast, handle_marlin_oob, write_marlin)) < 0) {
		result = transp_fd;
		fprintf(stderr, "Error %d creating transport instance\n", result);
		goto err_transport;
	}
	transp_motionq_fd   = transport_motionq_fd(transport);
	transp_inflightq_fd = transport_inflightq_fd(transport);

	/* Initialize the sessions */
	for (session_ndx = 0; session_ndx < ARRAY_SIZE(sessions); session_ndx++)
		if ((result = init_session_queue(&sessions[session_ndx])) < 0)
			goto err_session;

	/* Create an eventfd for the number of used reply bytes */
	if ((rplq.read_fd = eventfd(0, EFD_SEMAPHORE)) == -1) {
		perror("Error in-flight queue free eventfd");
		result = -errno;
		goto err_rplq_read_eventfd;
	}
	/* Create an eventfd for the number of free reply bytes */
//	if ((rplq.write_fd = eventfd(MAX_REPLY_DATA, EFD_SEMAPHORE)) == -1) {
//		perror("Error in-flight queue free eventfd");
//		result = -errno;
//		goto err_rplq_write_eventfd;
//	}

	/* Create a listener instance */
	if ((result = listener_new(&listener, NULL, SERVER_TCP_PORT, handle_client_connected)) < 0) {
		fprintf(stderr, "Error %d creating listener instance\n", result);
		goto err_listener;
	}

	return 0;

	listener_delete(&listener);
err_listener:
//	close(rplq.write_fd);
//err_rplq_write_eventfd:
	close(rplq.read_fd);
err_rplq_read_eventfd:
err_session:
	while(--session_ndx >= 0)
		term_session_queue(&sessions[session_ndx]);
	transport_delete(&transport);
err_transport:
err_set_perf_timer:
	close(perf_timer_fd);
err_perf_timer:
#ifndef __x86_64__
	tcsetattr(tty_fd, TCSANOW, &tty_oldtio);
err_setattr:
err_flush:
err_getattr:
err_verifybitrate:
	ioctl(tty_fd, TIOCGSERIAL, &tty_oldserinfo);
err_setbitrate:
err_backup:
	close(file);
	tty_fd = 0;
err_file:
#endif /* !__x86_64__ */
err_param:
	return result;
}


/*
 * This function is called once, to terminate all subsystems.
 */
static void term(void)
{
	int			session_ndx = ARRAY_SIZE(sessions);

	listener_delete(&listener);

//	close(rplq.write_fd);
	close(rplq.read_fd);

	while(--session_ndx >= 0)
		term_session_queue(&sessions[session_ndx]);

	transport_delete(&transport);

	close(perf_timer_fd);
#ifndef __x86_64__
	/* Restore the old attributes */
	tcsetattr(tty_fd, TCSANOW, &tty_oldtio);

	/* Restore the old serial configuration */
	ioctl(tty_fd, TIOCSSERIAL, &tty_oldserinfo);

	/* Flush the buffer */
	tcflush (tty_fd, TCIFLUSH);

	/* Close the serial port */
	close (tty_fd);
	tty_fd = 0;

	/* Hold Marlin in reset */
	reset_marlin(1);
#endif /* !__x86_64__ */
	/* TODO: closelog() & sync() when using syslog */
	return;
}


/*
 * This function installed as an on-exit handler, and is called automatically
 * whenever the process exits normally.
 */
static void exit_handler(int ev, void *arg)
{
	term();
}


/*
 * This function installed as a signal handler, and is called automatically
 * whenever a SIGINT, SIGQUIT, SIGTERM or SIGSEGV is received. Its purpose is
 * to exit normally, enabling the terminator to shut down all subsystems and to
 * pull Marlin into reset.
 * Handling of SIGSEGV is a special one. Because printing is not recommended
 * within a signal handler, the back-trace is stored and a long jump to main
 * is made, so it can be printed in main context.
 */
static void signal_handler(int signum)
{
	switch (signum) {
	case SIGINT:
	case SIGQUIT:
	case SIGTERM:
		/* Exit gracefully */
		exit(signum);
		break;
	case SIGSEGV:
		/* Save the trace in statics */
		bt_count = backtrace(bt_trace, ARRAY_SIZE(bt_trace));

		/* Jump to main context */
		longjmp(bt_env, signum);
	}
}


/*****************************************************************************/
/*** Functions								   ***/
/*****************************************************************************/
int main(int argc, char* argv[])
{
	int			arg;
	int			signum;
	struct pollfd		fds[3 + ARRAY_SIZE(sessions) + 3 + 1];
	/* Flags used to move commands from the session queues to Marlin transport layer */
	unsigned char		inflight_write_available = 0;
	unsigned char		motionq_write_available = 0;
	int			motion_command_session_ndx = -1;
	int			motion_command_session_ndx_next = 0;
	int			command_session_ndx = -1;
	int			command_session_ndx_next = 0;
	/* Flags used to move Marlin replies to the client TCP socket */
	unsigned char		reply_read_available = 0;
	unsigned char		client_write_available = 0;
	int			session_ndx;
#ifdef POLL_DEBUG
	int			index;
#endif /* POLL_DEBUG */
	int			result = 0;

	/* Install signal handler */
	signal(SIGINT,  signal_handler);
	signal(SIGQUIT, signal_handler);
	signal(SIGTERM, signal_handler);
	signal(SIGSEGV, signal_handler);
	signal(SIGPIPE, SIG_IGN); /* Ignore SIGPIPE */

	/* Create an environment copy to allow jumping here from signal handler, decoupling calls to syslog, sync et all */
	if ((signum = setjmp(bt_env)) != 0) {
		char		**strings = backtrace_symbols(bt_trace, bt_count);
		size_t		index;

		fprintf(stderr, "Caught signal %d\n", signum);
		for (index = 0; index < bt_count; index++)
			fprintf(stderr, "Back-trace: %s\n", strings[index]);
		exit(EXIT_FAILURE);
	}

	/* Initialize non-zero defaults */
	ttyport  = DEFAULT_TTY;
	bitrate  = DEFAULT_BITRATE;

	/* Process the command line arguments */
	while ((arg = getopt(argc, argv, "b:fp:vh")) != -1) {
		switch (arg) {
		case 'b':
			/* Retrieve bit rate */
			bitrate = strtol(optarg, NULL, 0);
			break;
		case 'f':
			/* Enable flow control */
			flowcontrol = 1;
			break;
		case 'p':
			/* Retrieve port */
			ttyport = optarg;
			break;
		case 'v':
			/* Be verbose */
			verbosity++;
			break;
		default:
			result = -1;
		case 'h':
			fprintf(stderr, "Usage: %s\n", basename(argv[0]));
			fprintf(stderr, "-b bit rate        Specify bitrate\n");
			fprintf(stderr, "-f                 Enable flow control\n");
			fprintf(stderr, "-p serial port     Specify serial port\n");
			fprintf(stderr, "-v                 Be verbose\n");
			fprintf(stderr, "-h                 Show this help\n");
			return result;
		}
	}
	argc -= optind;
	argv += optind;

	if (argc) {
		fprintf(stderr, "Error: Positional arguments are not supported\n");
		exit(-1);
	}

	/* Initialize all subsystems and install the exit handler to properly terminat under all cicumstances */
	if (init() < 0)
		exit(-1);
	on_exit(exit_handler, NULL);

	/* Setup fd array for poll */
	memset(&fds, 0, sizeof(fds));
	fds[0].fd      = tty_fd;
	fds[0].events |= POLLIN;
	fds[1].fd      = transp_inflightq_fd;
	fds[1].events |= POLLIN;
	fds[2].fd      = transp_motionq_fd;
	fds[2].events |= POLLIN;
	for (session_ndx = 0; session_ndx < ARRAY_SIZE(sessions); session_ndx++) {
		fds[3 + session_ndx].fd      = sessions[session_ndx].command_queue.read_fd;
		fds[3 + session_ndx].events |= POLLIN;
	}
	fds[3 + ARRAY_SIZE(sessions) + 0].fd      = rplq.read_fd;
	fds[3 + ARRAY_SIZE(sessions) + 0].events |= POLLIN;
	fds[3 + ARRAY_SIZE(sessions) + 1].fd      = -1;
	fds[3 + ARRAY_SIZE(sessions) + 1].events |= POLLIN;
	fds[3 + ARRAY_SIZE(sessions) + 2].fd      = transp_fd;
	fds[3 + ARRAY_SIZE(sessions) + 2].events |= POLLIN;
	fds[3 + ARRAY_SIZE(sessions) + 3].fd      = perf_timer_fd;
	fds[3 + ARRAY_SIZE(sessions) + 3].events |= POLLIN;

	/* Main control loop */
	for (;;) {
		if (poll(fds, ARRAY_SIZE(fds), -1) == -1) {
			perror("Error polling timer, socket and serial port");
			result = -errno;
			break;
		}

		/*
		 * Priority 1: Move data from Marlin serial port to to Marlin transport layer
		 */
		if (fds[0].revents & POLLIN) {
			unsigned char	buf[512];
			ssize_t		bytes;

			/* Read data from the serial port */
			if ((bytes = read(tty_fd, buf, sizeof(buf))) == -1) {
				perror("Error receiving tty data");
				result = -errno;
				break;
			}
			/* Process the data into packets */
			if ((result = transport_process(transport, buf, bytes)) < 0)
				/* Error reported in transport_process already */
				break;
			/* Continue priority 1 */
			continue;
		}

		/*
		 * Priority 2: Move commands from session queues to Marlin transport layer
		 */
		/* Check Marlin in-flight queue availability */
		if (fds[1].revents & POLLIN) {
			/* Activate Marlin in-flight queue availability flag and stop watching */
			fds[1].events &= ~POLLIN;
			inflight_write_available = 1;
		}

		/* Check Marlin motion queue availability */
		if (fds[2].revents & POLLIN) {
			/* Activate Marlin motion queue availability flag and stop watching */
			fds[2].events &= ~POLLIN;
			motionq_write_available = 1;
		}

		/* Check all sessions for motion command availability */
		if (motion_command_session_ndx == -1) {
			int			session_count = ARRAY_SIZE(sessions);

			/*
			 * More than one session may have a motion command available. To ensure equal
			 * opportunities between sessions, finding a session with a motion command
			 * available has to start at the last session checked.
			 */
			do {
				if (fds[3 + motion_command_session_ndx_next].revents & POLLIN) {
					struct command_queue_t	*command_queue = &sessions[motion_command_session_ndx_next].command_queue;
					struct command_entry_t	*entry = &command_queue->entry[command_queue->tail];

					/* Copy the session index and stop watching */
					if (entry->is_motion) {
						motion_command_session_ndx = motion_command_session_ndx_next;
						fds[3 + motion_command_session_ndx_next].events &= ~POLLIN;
					}
				}

				/* Advance the index to the session to be checked next */
				if (++motion_command_session_ndx_next >= ARRAY_SIZE(sessions))
					motion_command_session_ndx_next = 0;
			} while (--session_count && motion_command_session_ndx == -1);
		}

		/* Check all sessions for non-motion command availability TODO: This is almost the same as for a motion command, so a function would be in place */
		if (command_session_ndx == -1) {
			int			session_count = ARRAY_SIZE(sessions);

			/*
			 * More than one session may have a non-motion command available. To ensure equal
			 * opportunities between sessions, finding a session with a non-motion command
			 * available has to start at the last session checked.
			 */
			do {
				if (fds[3 + command_session_ndx_next].revents & POLLIN) {
					struct command_queue_t	*command_queue = &sessions[command_session_ndx_next].command_queue;
					struct command_entry_t	*entry = &command_queue->entry[command_queue->tail];

					/* Copy the session index and stop watching */
					if (!entry->is_motion) {
						command_session_ndx = command_session_ndx_next;
						fds[3 + command_session_ndx_next].events &= ~POLLIN;
					}
				}

				/* Advance the index to the session to be checked next */
				if (++command_session_ndx_next >= ARRAY_SIZE(sessions))
					command_session_ndx_next = 0;
			} while (--session_count && command_session_ndx == -1);
		}

		/* First, check flags to determine if a motion command can be sent */
		if (motion_command_session_ndx != -1 && inflight_write_available && motionq_write_available) {
			struct session_t	*session = &sessions[motion_command_session_ndx];
			struct command_queue_t	*command_queue = &session->command_queue;
			struct command_entry_t	*entry = &command_queue->entry[command_queue->tail];
			eventfd_t		dummy;

			/* Take the command queue read semaphore */
			if (eventfd_read(command_queue->read_fd, &dummy) == -1) {
				perror("Error reading command queue read eventfd");
				exit(1);
			}
			/* Re-enable poll for motion command read availability */
			fds[3 + motion_command_session_ndx].events |= POLLIN;
			motion_command_session_ndx = -1;

			/* Send the new motion command */
			transport_send(transport, MOTION_CMD, session, 0, entry->gcode, entry->gcode_len);

			/* Dequeue the motion command for the command queue */
			if (++command_queue->tail > MAX_CMDS)
				command_queue->tail = 0;
			/* Release the command queue write semaphore, enabling a new write to the command queue */
			if (eventfd_write(command_queue->write_fd, 1) == -1) {
				perror("Error writing command queue write eventfd");
				exit(1);
			}

			/* Re-enable poll for in-flight queue availability */
			inflight_write_available = 0;
			fds[1].events |= POLLIN;
			/* Re-enable poll for motion queue availability */
			motionq_write_available = 0;
			fds[2].events |= POLLIN;

			/* Continue priority 2 (or higher) */
			continue;
		}

		/* Second, check flags to determine if a non-motion command can be sent TODO: This is almost the same as for a motion command, so a function would be in place */
		if (command_session_ndx != -1 && inflight_write_available) {
			struct session_t	*session = &sessions[command_session_ndx];
			struct command_queue_t	*command_queue = &session->command_queue;
			struct command_entry_t	*entry = &command_queue->entry[command_queue->tail];
			eventfd_t		dummy;

			/* Take the command queue read semaphore */
			if (eventfd_read(command_queue->read_fd, &dummy) == -1) {
				perror("Error reading command queue read eventfd");
				exit(1);
			}
			/* Re-enable poll for command read availability */
			fds[3 + command_session_ndx].events |= POLLIN;
			command_session_ndx = -1;

			/* Send the new command */
			transport_send(transport, NO_MOTION_CMD, session, entry->refnr, entry->gcode, entry->gcode_len);

			/* Dequeue the command for the command queue */
			if (++command_queue->tail > MAX_CMDS)
				command_queue->tail = 0;
			/* Release the command queue write semaphore, enabling a new write to the command queue */
			if (eventfd_write(command_queue->write_fd, 1) == -1) {
				perror("Error writing command queue write eventfd");
				exit(1);
			}

			/* Re-enable poll for in-flight queue availability */
			inflight_write_available = 0;
			fds[1].events |= POLLIN;
			/* Re-enable poll for motion queue availability */
			motionq_write_available = 0;
			fds[2].events |= POLLIN;

			/* Continue priority 2 (or higher) */
			continue;
		}

		/*
		 * Priority 4: Move data from reply data queue to the client TCP socket
		 */
		/* TODO: Replies arrive in handler given in transport_send, called from transport_process. And to complicate things: We don't know how long replies are */
		if (fds[3 + ARRAY_SIZE(sessions)].revents & POLLIN) {
			fds[3 + ARRAY_SIZE(sessions)].events &= ~POLLIN;
			reply_read_available = 1;
		}

		if (fds[3 + ARRAY_SIZE(sessions) + 1].revents & POLLOUT) {
			fds[3 + ARRAY_SIZE(sessions) + 1].events &= ~POLLOUT;
			client_write_available = 1;
		}

		if (reply_read_available && client_write_available) {
//			int		read_available;
//			int		write_available;
//			int		index;

//			if (ioctl(fds[3 + ARRAY_SIZE(session) + 1].fd, FIONSPACE, &write_available) < 0) {
//				perror("Error reading space available from network socket");
//				result = -errno;
//				break;
//			}
//			if (ioctl(fds[3 + ARRAY_SIZE(session)].fd, FIONREAD, &read_available) < 0) {
//				perror("Error reading bytes available from reply queue read eventfd");
//				result = -errno;
//				break;
//			}

//			if (read_available > write_available)
//				read_available = write_available;

//			for (index = 0; index < read_available; index++) {
			for (;;) {
				eventfd_t		dummy;

				/* Take the reply queue read semaphore */
				if (eventfd_read(rplq.read_fd, &dummy) == -1) {
					perror("Error reading reply queue read eventfd");
					exit(1);
				}
				write(fds[3 + ARRAY_SIZE(sessions) + 1].fd, &rplq.data[rplq.tail], 1);
				if (++rplq.tail > MAX_REPLY_DATA)
					rplq.tail = 0;
				if (rplq.tail == rplq.head)
					break;
			}

			reply_read_available = 0;
			fds[3 + ARRAY_SIZE(sessions)].events |= POLLIN;
			client_write_available = 0;
			fds[3 + ARRAY_SIZE(sessions) + 1].events |= POLLOUT;

			/* Continue priority 4 (or higher) */
			continue;
		}

		/*
		 * Handle transport layer background work
		 */
		if (fds[3 + ARRAY_SIZE(sessions) + 2].revents & POLLIN) {
			if ((result = transport_worker(transport)) < 0)
				/* Error reported in transport_worker already */
				break;
			continue;
		}

		if (fds[3 + ARRAY_SIZE(sessions) + 3].revents & POLLIN) {
			unsigned long long	missed;

			if (read(perf_timer_fd, &missed, sizeof(missed)) < 0) {
				perror("Error reading performance timer");
				result = -errno;
				break;
			}

			fprintf(stderr, "Performance:");
			for (session_ndx = 0; session_ndx < ARRAY_SIZE(sessions); session_ndx++) {
				fprintf(stderr, " [%d] = %d packets/s%s", session_ndx, sessions[session_ndx].perf_counter, (session_ndx < ARRAY_SIZE(sessions)-1)?",":"");
				sessions[session_ndx].perf_counter = 0;
			}
			fprintf(stderr, "\n");
			continue;
		}

#ifdef POLL_DEBUG
		for (index = 0; index < ARRAY_SIZE(fds); index++)
			if (fds[index].revents)
				fprintf(stderr, "fds[%d].revents = 0x%.8x\n", index, fds[index].revents);
#endif /* POLL_DEBUG */
	}

	return result;
}
