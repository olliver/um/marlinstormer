#ifndef INFLIGHTQ_H				/* Include file already compiled? */
#define INFLIGHTQ_H


size_t inflightq_process(void *inflightq, void *data, size_t length);

int inflightq_size(void *inflightq);
int inflightq_used(void *inflightq);
int inflightq_used_motions(void *inflightq);
void inflightq_queue(void *inflightq, unsigned char seq_nr, unsigned char is_motion, void *user_context, uint32_t user_refnr, void *data, size_t length);
void inflightq_resend(void *inflightq);
void inflightq_dequeue(void *inflightq);
unsigned int inflightq_seqnr(void *inflightq);
void *inflightq_tail_user_context(void *inflightq);
uint32_t inflightq_tail_user_refnr(void *inflightq);

/* Hack/debug functions */
unsigned long inflightq_tailage_ms(void *inflightq);
unsigned char *inflightq_tailcmd(void *inflightq);
size_t inflightq_tailcmdlen(void *inflightq);

int inflightq_new(void    **inflightq, void *user_data,
		  void    (*handle_packet)(void *user_data, unsigned char seq_nr, unsigned char packet_type, void *data, unsigned char data_len),
		  void    (*handle_oob)(void *user_data, void *data, unsigned char data_len),
		  ssize_t (*write_packet)(void *user_data, void *packet, ssize_t packet_len));
void inflightq_delete(void **inflightq);


#endif /* INFLIGHTQ_H */
