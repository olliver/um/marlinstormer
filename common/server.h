#ifndef SERVER_H				/* Include file already compiled? */
#define SERVER_H


enum reason_t {
	CONN_CLOSED,		/* Connection closed normally */
	CONN_DISCONNECTED,	/* Connection closed purposely by server */
	CONN_TIMEOUT,		/* Connection closed by timeout */
	CONN_LOST,		/* Connection closed by read error */
	CONN_ERROR,		/* Connection closed by error */
	CONN_QUIT		/* Connection closed by quitting */
};


ssize_t server_write	(void			*server,
			 void			*data,
			 size_t			len,
			 unsigned char		flush);

int  server_new		(void			**server,
			 void			*user_data,
			 char			*client_ip,
			 int			csock,
			 void			(*data_handler)(void *user_data, unsigned char *data, size_t len),
			 void			(*disconn_handler)(void *user_data, enum reason_t reason));
void server_delete	(void			**server);


#endif /* SERVER_H */
