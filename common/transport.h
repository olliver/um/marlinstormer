#ifndef TRANSPORT_H				/* Include file already compiled? */
#define TRANSPORT_H


#define NO_MOTION_CMD		0
#define MOTION_CMD		1


void transport_get_queueinfo(void *transport, unsigned char *inflight_size, unsigned char *inflight_used, unsigned char *motion_size, unsigned char *motion_used);
void transport_send(void *transport, unsigned char is_motion, void *user_context, uint32_t user_refnr, void *data, size_t length);
size_t transport_process(void *transport, void *data, size_t length);
int transport_motionq_fd(void *transport);
int transport_inflightq_fd(void *transport);
int transport_worker(void *transport);

int transport_new(void    **transport,
		  void    (*handle_reply)(void *user_context, uint32_t user_refnr, char *data, size_t length),
		  void    (*handle_broadcast)(char *data, size_t length),
		  void    (*handle_oob)(void *data, unsigned char data_len),
		  ssize_t (*write_packet)(void *packet, ssize_t packet_len));
void transport_delete(void **transport);

#endif /* TRANSPORT_H */
