#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>

void hexdump(FILE *stream, void *addr, size_t len)
{
	unsigned char	*data = (unsigned char *)addr;
	size_t		index = 0;

	fprintf(stream, "data @%p, %d bytes:\n", data, (int)len);

	while (index < len) {
		size_t	byte;
		char	buffer[16];
		char	line[81] = {0};

		/* Address */
		sprintf(buffer, "data[0x%.4x]: ", (int)index);
		strcat(line, buffer);

		/* Bytes in HEX */
		byte = 0;
		while (byte < 16) {
			if ((index + byte) < len) {
				sprintf(buffer, " %.2x", data[index + byte]);
				strcat(line, buffer);
			} else
				strcat(line, "   ");
			byte++;
		}

		/* Separator */
		strcat(line, "  ");

		/* Bytes in ASCII */
		byte = 0;
		while ((byte < 16) && ((index + byte) < len)) {
			if ((data[index + byte] >= ' ') &&
					(data[index + byte] <= '~')) {
				sprintf(buffer, "%c", data[index + byte]);
				strcat(line, buffer);
			} else
				strcat(line, ".");
			byte++;
		}

		/* Line ending */
		fprintf(stream, "%s\n", line);
		line[0] = 0;
		/* Next line */
		index += 16;
	}
}
