#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>


/*****************************************************************************/
/*** Macros								   ***/
/*****************************************************************************/
#define BLOCKSIZE	512


/*****************************************************************************/
/*** Types								   ***/
/*****************************************************************************/
struct linereader_t {
	int	fd;

	size_t	size;
	size_t	index;
	char	data[BLOCKSIZE];
};


/*****************************************************************************/
/*** Functions								   ***/
/*****************************************************************************/
int linereader_read(void *linereader, void *buf, size_t size)
{
	struct linereader_t	*this = (struct linereader_t *)linereader;
	size_t			copied = 0;

	/* Copy the line */
	while (copied < size) {
		/* Check if a new block needs to be read */
		if (this->index >= this->size) {
			ssize_t		bytes;

			/* Read a new block (not byte-by-byte: Read syscalls are expensive!) */
			if ((bytes = read(this->fd, this->data, sizeof(this->data))) < 0) {
				perror("Error reading line");
				return -errno;
			}
			if (!bytes)
				goto err_noend;
			this->size = bytes;
			this->index = 0;
		}

		/* Check if we have found the end of the line */
		if (this->data[this->index] == '\n') {
			((char *)buf)[copied] = '\0';
			this->index++;
			/* Return what's copied so far */
			return 0;
		}

		/* Copy the data byte TODO: Use memccopy */
		((char *)buf)[copied] = this->data[this->index];
		copied++;
		this->index++;
	}
err_noend:
	fprintf(stderr, "No line end found\n");
	return -1;
}


int linereader_new(void    **linereader, char *file_name)
{
	struct linereader_t	*this;
	int			result = 0;
	ssize_t			bytes;

	/* Allocate memory for this new instance */
	if (!(this = calloc(1, sizeof(struct linereader_t)))) {
		result = -ENOMEM;
		goto err_mem;
	}

	/* Initialize new linereader data */
	if ((this->fd = open(file_name, O_RDONLY)) < 0)
	{
		perror("Error opening model file");
		result = -errno;
		goto err_open;
	}
	if ((bytes = read(this->fd, this->data, sizeof(this->data))) < 0) {
		perror("Error reading model file");
		result = -errno;
		goto err_read;
	}
	this->size = bytes;

	/* Fill out caller's pointer */
	*linereader = this;

	return 0;
err_read:
	close(this->fd);
err_open:
	free(this);
err_mem:
	return result;
}


void linereader_delete(void **linereader)
{
	struct linereader_t	*this = (struct linereader_t *)*linereader;

	/* Invalidate caller's pointer first */
	*linereader = NULL;

	if (!this) {
		fprintf(stderr, "Attempt to delete a non-existing linereader\n");
		return;
	}

	/* Close the file descriptor */
	close(this->fd);

	/* Free the memory */
	free(this);
}

