#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <poll.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <errno.h>
#include <netinet/tcp.h>
#include <sys/timerfd.h>
#include <sys/ioctl.h>

#include "hexdump.h"
#include "server.h"


/*****************************************************************************/
/*** Macros								   ***/
/*****************************************************************************/
#define TIMEOUT_S		(30)
#define BUFFER_SIZE		(4 * 1024)

#define ARRAY_SIZE(arr)		(sizeof(arr) / sizeof((arr)[0]))


/*****************************************************************************/
/*** Types								   ***/
/*****************************************************************************/
struct server_t {
	/* Properties initialized only once */
	void			*user_data;				/* User_data provided to data_handler function */
	char			client_ip[INET_ADDRSTRLEN];		/* String holding the currently connected client's IP address */
	void			(*data_handler)(void *user_data, unsigned char *data, size_t len); /* Call-back function for incoming data */
	void			(*disconn_handler)(void *user_data, enum reason_t reason); /* Call-back function for connection change */

	pthread_t		pthread;				/* Pthread to handle incoming connections */
	int			pipe[2];				/* Pipe to communicate with pthread */

	/* Client socket and file handle */
	int			csock;

	/* Connection timeout */
	int			timerfd;				/* Connection timeout file descriptor */
	struct itimerspec	itval;					/* Connection timeout value */

	unsigned char		buffer[BUFFER_SIZE];
};


/*****************************************************************************/
/*** Globals								   ***/
/*****************************************************************************/


/*****************************************************************************/
/*** Static functions							   ***/
/*****************************************************************************/
static void *server_thread(void *arg)
{
	struct server_t		*this = (struct server_t *)arg;
	struct pollfd		fds[3];
	enum reason_t		reason = CONN_ERROR;
	unsigned char		shutdown_and_close = 1;

	/* Setup fd array for poll */
	memset(fds, 0 , sizeof(fds));
	fds[0].fd     = this->pipe[0];
	fds[0].events = POLLIN;
	fds[1].fd     = this->csock;
	fds[1].events = POLLIN;
	fds[2].fd     = this->timerfd;
	fds[2].events = POLLIN;

	/* Read incoming data */
	for (;;) {
		/* Wait for data or pipe */
		if (poll(fds, ARRAY_SIZE(fds), -1) < 0) {
			perror("Error polling server");
			break; /* Break from connection loop */
		}

		/* Check if we have any messages */
		if (fds[0].revents & POLLIN) {
			ssize_t		bytes;
			char		message[1];

			if ((bytes = read(this->pipe[0], message, sizeof(message))) == -1) {
				perror("Error reading server message pipe");
				break; /* Break from connection loop */
			}
			/* Check if we are told to quit */
			if (message[bytes-1] == 'Q') {
				fprintf(stderr, "Quitting server thread (%s-%d)\n", this->client_ip, this->csock);
				reason = CONN_QUIT;
				break; /* Break from connection loop */
			}
			/* Check if we are told to disconnect */
			if (message[bytes-1] == 'D') {
				fprintf(stderr, "Disconnecting (%s-%d)\n", this->client_ip, this->csock);
				reason = CONN_DISCONNECTED;
				break; /* Break from connection loop */
			}
			fprintf(stderr, "Warning: Received unsupported message 0x%.2x through server message pipe (%s-%d)\n", message[bytes-1], this->client_ip, this->csock);
			continue; /* Back to poll */
		}

		if (fds[1].revents & POLLHUP) {
			fprintf(stderr, "Error POLLHUP server read (%s-%d)\n", this->client_ip, this->csock);
			reason = CONN_CLOSED;
			break; /* Break from connection loop */
		}
		if (fds[1].revents & POLLERR) {
			fprintf(stderr, "Error POLLERR server read (%s-%d)\n", this->client_ip, this->csock);
			reason = CONN_LOST;
			break; /* Break from connection loop */
		}
		if (fds[1].revents & POLLNVAL) {
			fprintf(stderr, "Error POLLNVAL server read (%s-%d)\n", this->client_ip, this->csock);
			shutdown_and_close = 0;
			break; /* Break from connection loop */
		}

		/* Check if we have data */
		if (fds[1].revents & POLLIN) {
			ssize_t		bytes;

			/* Read data from the client socket */
			bytes = read(this->csock, this->buffer, sizeof(this->buffer));
			if (bytes > 0) {
				if (this->data_handler)
					this->data_handler(this->user_data, this->buffer, bytes);

				/* Update timeout */
				if (timerfd_settime(this->timerfd, 0, &this->itval, NULL) < 0) {
					perror("Error updating server timeout timer");
					break; /* Break from connection loop */
				}
			} else if (!bytes) {
				reason = CONN_CLOSED;
				break; /* Break from connection loop */
			} else {
				/* Shouldn't happen; Timeout should catch it first */
				reason = CONN_LOST;
				break; /* Break from connection loop */
			}
			continue; /* Back to poll */
		}

		/* Check if we have a timeout */
		if (fds[2].revents & POLLIN) {
			unsigned long long	missed;
			ssize_t			bytes;

			if ((bytes = read(this->timerfd, &missed, sizeof(missed))) < 0) {
				perror("Error reading client timeout timer");
				break; /* Break from connection loop */
			} else if (bytes) {
				fprintf(stderr, "Error timeout reading client (%s-%d)\n", this->client_ip, this->csock);
				reason = CONN_TIMEOUT;
				break; /* Break from connection loop */
			}
			continue; /* Back to poll */
		}

		fprintf(stderr, "Error unhandled poll event fds[0].revents = 0x%.8x fds[1].revents = 0x%.8x fds[2].revents = 0x%.8x (%s-%d)\n",
				fds[0].revents, fds[1].revents, fds[2].revents, this->client_ip, this->csock);
	}

	/* Notify about disconnection */
	if (this->disconn_handler)
		this->disconn_handler(this->user_data, reason);

	if (shutdown_and_close) {
		shutdown(this->csock, SHUT_RDWR);
		close(this->csock);
	}

	/* Teardown */
	close(this->pipe[1]);
	close(this->pipe[0]);
	shutdown(this->csock, SHUT_RDWR);
	close(this->csock);
	free(this);
	return 0;
}


/*****************************************************************************/
/*** Functions								   ***/
/*****************************************************************************/
ssize_t server_write(void *server, void *data, size_t len, unsigned char flush)
{
	struct server_t		*this = (struct server_t *)server;
	size_t			bytes;

	if (!this) {
		fprintf(stderr, "Attempt to write to a non-existing server\n");
		return -1;
	}

	bytes = write(this->csock, data, len);
	if (bytes == -1) {
		if (errno != EAGAIN && errno != EWOULDBLOCK) {
			perror("Error writing data to server socket");
			return -errno;
		}

		fprintf(stderr, "Buffer full writing %d bytes to server socket: trashing all\n", (int)len);
		return 0;
	}

	if (bytes < len)
		/* Caveat: Like write's return value, variable 'bytes' is unsigned, so a return value of -1 is not detected by (bytes < len) */
		/* Note: By the time this happens, the client is already ~256kB behind reading */
		fprintf(stderr, "Buffer full writing %d bytes to server socket: trashing %d bytes\n", (int)len, (int)len - bytes);

	return bytes;
}


int server_new(void **server, void *user_data, char *client_ip, int csock,
	       void (*data_handler)(void *user_data, unsigned char *data, size_t len),
	       void (*disconn_handler)(void *user_data, enum reason_t reason))
{
	struct server_t		*this;
	int			result = 0;

	/* Allocate memory for this new server */
	if (!(this = malloc(sizeof(struct server_t)))) {
		result = -ENOMEM;
		fprintf(stderr, "Error allocating memory for server (%s-%d)\n", client_ip, csock);
		goto err_mem;
	}
	memset(this, 0, sizeof(struct server_t));

	/* Initialize new server data */
	this->user_data       = user_data;
	this->csock           = csock;
	this->data_handler    = data_handler;
	this->disconn_handler = disconn_handler;
	strncpy(this->client_ip, client_ip, sizeof(this->client_ip));

	/* Create connection timeout timer */
	if ((this->timerfd = timerfd_create(CLOCK_MONOTONIC, 0)) == -1) {
		perror("Error creating server connection timeout timer");
		result = -errno;
		goto err_timer;
	}

	/* Fill out timer value */
	memset(&this->itval, 0, sizeof(this->itval));
	this->itval.it_value.tv_sec  = TIMEOUT_S;
	this->itval.it_value.tv_nsec = 0;

	/* Set initial connection timeout */
	if (timerfd_settime(this->timerfd, 0, &this->itval, NULL) < 0) {
		perror("Error setting server connection timeout timer");
		result = -errno;
		goto err_settimer;
	}

	/* Create a communications pipe for the thread */
	if (pipe(this->pipe) < 0) {
		perror("Error initializing server pipe");
		result = -errno;
		goto err_pipe;
	}

	/* Fill out caller's pointer */
	*server = this;

	/* Create a listener thread */
	if ((result = pthread_create(&this->pthread, NULL, &server_thread, this)) < 0) {
		fprintf(stderr, "Error %d creating server thread\n", result);
		goto err_thread;
	}

	fprintf(stderr, "Created server (%s-%d)\n", client_ip, csock);
	return 0;
err_thread:
	*server = NULL;
	close(this->pipe[1]);
	close(this->pipe[0]);
err_pipe:
err_settimer:
	close(this->timerfd);
err_timer:
	free(this);
err_mem:
	return result;
}


void server_delete(void **server)
{
	struct server_t		*this = (struct server_t *)*server;
	ssize_t			bytes;

	/* Invalidate caller's pointer first */
	*server = NULL;

	if (!this) {
		fprintf(stderr, "Attempt to delete a non-existing server\n");
		return;
	}

	/* Tell pthread to quit */
	if ((bytes = write(this->pipe[1], "Q", 1)) <= 0) {
		perror("Error stopping server pthread, canceling it instead");
		pthread_cancel(this->pthread);

		/* Teardown */
		close(this->pipe[1]);
		close(this->pipe[0]);
		shutdown(this->csock, SHUT_RDWR);
		close(this->csock);
		free(this);
	} else
		/* Wait for the pthread to finish */
		pthread_join(this->pthread, NULL);
}
