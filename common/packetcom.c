/*****************************************************************************/
/*** File:	packetcom.c						   ***/
/*** Developer:	Robert Delien (robert@delien.nl)			   ***/
/***		Copyright (C) 2013, Clockwork Engineering		   ***/
/***									   ***/
/*** Function:	This file reads a file descriptor, detecting packets and   ***/
/***		wraps data in a packet to send it to a file descriptor.	   ***/
/***									   ***/
/*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <arpa/inet.h>
#include <stddef.h>

#include "crc8.h"
#include "packetcom.h"


/*****************************************************************************/
/*** Macros								   ***/
/*****************************************************************************/
#define SYNC_BYTE		0xff
#define TX_MAX_PAYLOAD		96

#define PACKETOVERHEAD		(sizeof(struct packet_t) - UCHAR_MAX)


/*****************************************************************************/
/*** Types								   ***/
/*****************************************************************************/
struct packet_t
{
	unsigned char	sync;
	unsigned char	seq_nr;
	unsigned char	type;
	unsigned char	payload_size;
	unsigned char	payload[UCHAR_MAX];
	unsigned char	crc8;
} __attribute__((packed));



struct packetcom_t {
	int		state;
	void		*user_data;
	void		(*handle_packet)(void *user_data, unsigned char seq_nr, unsigned char packet_type, void *data, unsigned char data_len);
	void		(*handle_oob)(void *user_data, void *data, unsigned char data_len);
	ssize_t		(*write_packet)(void *user_data, void *packet, ssize_t packet_len);

	/* Receive buffer */
	union {
		struct packet_t	packet;
		unsigned char	bytes[sizeof(struct packet_t)];
	}		rcv_buf;
	size_t		rcv_buf_index;
};


/*****************************************************************************/
/*** Globals								   ***/
/*****************************************************************************/


/*****************************************************************************/
/*** Static functions							   ***/
/*****************************************************************************/


/*****************************************************************************/
/*** Functions								   ***/
/*****************************************************************************/
int packetcom_new(void    **packetcom, void *user_data,
		  void    (*handle_packet)(void *user_data, unsigned char seq_nr, unsigned char packet_type, void *data, unsigned char data_len),
		  void    (*handle_oob)(void *user_data, void *data, unsigned char data_len),
		  ssize_t (*write_packet)(void *user_data, void *packet, ssize_t packet_len))
{
	struct packetcom_t	*this_packetcom;
	int			result = 0;

	/* Allocate memory for this new instance */
	if (!(this_packetcom = calloc(1, sizeof(struct packetcom_t)))) {
		result = -ENOMEM;
		goto err_mem;
	}

	/* Initialize non-zero data */
	this_packetcom->user_data     = user_data;
	this_packetcom->handle_packet = handle_packet;
	this_packetcom->handle_oob    = handle_oob;
	this_packetcom->write_packet  = write_packet;

	/* Fill out caller's pointer */
	*packetcom = this_packetcom;

	return 0;
err_mem:
	return result;
}


void packetcom_delete(void **packetcom)
{
	struct packetcom_t	*this_packetcom = (struct packetcom_t *)*packetcom;

	/* Invalidate caller's pointer first */
	*packetcom = NULL;

	if (!this_packetcom) {
		fprintf(stderr, "Attempt to delete a non-existing packetcom\n");
		return;
	}

	/* Free the memory */
	free(this_packetcom);
}


size_t packetcom_process(void *packetcom, void *data, size_t length)
{
	struct packetcom_t	*this_packetcom = (struct packetcom_t *)packetcom;
	unsigned char		*data_byte = (unsigned char *)data;
	size_t			data_byte_index = 0;
	int			copylen;

	if (!this_packetcom) {
		fprintf(stderr, "Attempt to send data to a non-existing packetcom\n");
		return -1;
	}

	while (data_byte_index < length) {
		switch (this_packetcom->state) {
		default:
			this_packetcom->state = 0;

		case 0:		/* Idle; Wait for sync byte */
			if (data_byte[data_byte_index] == SYNC_BYTE) {
				this_packetcom->rcv_buf_index = 0;
				this_packetcom->rcv_buf.bytes[this_packetcom->rcv_buf_index] = data_byte[data_byte_index];
				this_packetcom->rcv_buf_index++;
				data_byte_index++;
				this_packetcom->state++;
				break;
			}
			/* Call the out-of-band data handler, if installed */
			if (this_packetcom->handle_oob)
				this_packetcom->handle_oob(this_packetcom->user_data, &data_byte[data_byte_index], 1);
			data_byte_index++;
			break;

		case 1:		/* Read Marlin packet sequence nr. */
		case 2:		/* Read Marlin packet type */
		case 3:		/* Read Marlin packet payload size */
			this_packetcom->rcv_buf.bytes[this_packetcom->rcv_buf_index] = data_byte[data_byte_index];
			this_packetcom->rcv_buf_index++;
			data_byte_index++;
			this_packetcom->state++;
			break;

		case 4:		/* Read payload */
			/* Determine how many payload bytes we (still) should copy */
			copylen = this_packetcom->rcv_buf.packet.payload_size - (this_packetcom->rcv_buf_index - offsetof(struct packet_t, payload));
			/* Limit to how many payload bytes are still in the given buffer */
			if (copylen > length - data_byte_index)
				copylen = length - data_byte_index;

			/* Copy the payload */
			memcpy(&this_packetcom->rcv_buf.bytes[this_packetcom->rcv_buf_index], &data_byte[data_byte_index], copylen);
			data_byte_index += copylen;
			this_packetcom->rcv_buf_index += copylen;

			/* Proceed to the next state if the payload is complete */
			if (this_packetcom->rcv_buf_index >= this_packetcom->rcv_buf.packet.payload_size + offsetof(struct packet_t, payload))
				this_packetcom->state++;
			break;

		case 5:		/* Read CRC byte */
			/* Verify the CRC */
			if (!crc8_verify(this_packetcom->rcv_buf.bytes,
			                 this_packetcom->rcv_buf.packet.payload_size + offsetof(struct packet_t, payload),
			                 data_byte[data_byte_index])) {
				/* Zero-terminate the payload to allow processing as a string without copying (Note: This will end up in the CRC location in case of maximum payload) */
				this_packetcom->rcv_buf.bytes[this_packetcom->rcv_buf_index] = '\0';
				/* Call the packet handler, if installed */
				if (this_packetcom->handle_packet)
					this_packetcom->handle_packet(this_packetcom->user_data, this_packetcom->rcv_buf.packet.seq_nr, this_packetcom->rcv_buf.packet.type, this_packetcom->rcv_buf.packet.payload, this_packetcom->rcv_buf.packet.payload_size);
			} else
				fprintf(stderr, "CRC error\n");

			/* Proceed to finding the next packet */
			this_packetcom->state = 0;
			data_byte_index++;
			break;
		}
	}

	return data_byte_index;
}


int packetcom_send(void *packetcom, unsigned char seq_nr, void *data, size_t length)
{
	struct packetcom_t	*this_packetcom = (struct packetcom_t *)packetcom;
	struct packet_t		packet;

	if (!this_packetcom) {
		fprintf(stderr, "Attempt to send data through a non-existing packetcom\n");
		return -1;
	}

	if (!this_packetcom->write_packet)
		return 0;

	if (length > TX_MAX_PAYLOAD) {
		fprintf(stderr, "Error sending %u byte packet (%u max.)\n", (unsigned int)length, TX_MAX_PAYLOAD);
		return 0;
	}

	/* Fill out preamble and length */
	packet.sync         = SYNC_BYTE;
	packet.seq_nr       = seq_nr;
	packet.type         = 'c';
	packet.payload_size = length;

	/*
	 * Copy the data into the packet payload
	 * NOTE: Copying is expensive, but in this case still cheaper than calling
	 *       this_packetcom->write_packet 3 times because that results in 3 expensive
	 *       system calls.
	 */
	memcpy(packet.payload, data, packet.payload_size);

	/* Calculate the CRC8 over the seq_nr, type, length and payload, and put it behind the payload */
	packet.payload[packet.payload_size] = crc8_calculate(&packet.seq_nr, sizeof(packet.seq_nr) + sizeof(packet.type) + sizeof(packet.payload_size) + packet.payload_size);

	/* Send out the packet */
	return this_packetcom->write_packet(this_packetcom->user_data, &packet, PACKETOVERHEAD + length);
}
