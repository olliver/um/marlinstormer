#ifndef LISTENER_H				/* Include file already compiled? */
#define LISTENER_H


int  listener_new	(void			**listener,
			 void			*user_data,
			 short			port_nr,
			 int			(*conn_handler)(void *user_data, char *client_ip, int client_fd));
void listener_delete	(void			**listener);


#endif /* LISTENER_H */
