#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <arpa/inet.h>
#include <stddef.h>

#include "packetizer.h"


/*****************************************************************************/
/*** Macros								   ***/
/*****************************************************************************/
#define SOH			0x01	/* ASCII: Start of heading */
#define PACKETOVERHEAD		(sizeof(struct packet_t) - UCHAR_MAX)


/*****************************************************************************/
/*** Types								   ***/
/*****************************************************************************/
struct packet_t
{
	unsigned char	soh;
	unsigned char	payload_size;
	unsigned char	payload[UCHAR_MAX];
} __attribute__((packed));

struct packetizer_t {
	int		state;
	void		*user_data;
	void		(*handle_packet)(void *user_data, void *packet, unsigned char packet_len);
	void		(*handle_oob)(void *user_data, void *data, unsigned char data_len);
	ssize_t		(*write_packet)(void *user_data, void *packet, ssize_t packet_len);

	/* Receive buffer */
	union {
		struct packet_t	packet;
		unsigned char	bytes[sizeof(struct packet_t)];
	}		rcv_buf;
	size_t		rcv_buf_index;
};


/*****************************************************************************/
/*** Functions								   ***/
/*****************************************************************************/
size_t packetizer_process(void *packetizer, void *data, size_t length)
{
	struct packetizer_t	*this = (struct packetizer_t *)packetizer;
	unsigned char		*data_byte = (unsigned char *)data;
	size_t			data_byte_index = 0;
	int			copylen;

	if (!this) {
		fprintf(stderr, "Attempt to send data to a non-existing packetizer\n");
		return -1;
	}

	while (data_byte_index < length) {
		switch (this->state) {
		default:
			this->state = 0;

		case 0:		/* Idle; Wait for Start of Heading */
			if (data_byte[data_byte_index] == SOH) {
				this->rcv_buf_index = 0;
				this->rcv_buf.bytes[this->rcv_buf_index++] = data_byte[data_byte_index++];
				this->state++;
				break;
			}
			/* Call the out-of-band data handler, if installed */
			if (this->handle_oob)
				this->handle_oob(this->user_data, &data_byte[data_byte_index], 1);
			data_byte_index++;
			break;

		case 1:		/* Read the payload size */
			this->rcv_buf.bytes[this->rcv_buf_index++] = data_byte[data_byte_index++];
			/* We're done if the the payload is 0 */
			if (!this->rcv_buf.packet.payload_size) {
				this->state = 0;
				break;
			}
			this->state++;
			break;

		case 2:		/* Read payload */
			/* Check if complete payload is within the given buffer */
			if (this->rcv_buf_index == offsetof(struct packet_t, payload) &&
			    this->rcv_buf.packet.payload_size <= (length - data_byte_index)) {
				/* Call the packet handler, if installed, directly with data from the given buffer */
				if (this->handle_packet)
					this->handle_packet(this->user_data, &data_byte[data_byte_index], this->rcv_buf.packet.payload_size);
				/* Continue behind the payload */
				data_byte_index += this->rcv_buf.packet.payload_size;
				/* Proceed to the next packet */
				this->state = 0;
				break;
			}

			/* Determine how many payload bytes we (still) should copy */
			copylen = this->rcv_buf.packet.payload_size - (this->rcv_buf_index - offsetof(struct packet_t, payload));
			/* Limit to how many payload bytes are still in the given buffer */
			if (copylen > length - data_byte_index)
				copylen = length - data_byte_index;

			/* Copy the payload */
			memcpy(&this->rcv_buf.bytes[this->rcv_buf_index], &data_byte[data_byte_index], copylen);
			data_byte_index += copylen;
			this->rcv_buf_index += copylen;

			/* Check if the payload is complete */
			if (this->rcv_buf_index >= this->rcv_buf.packet.payload_size + offsetof(struct packet_t, payload)) {
				/* Call the packet handler, if installed */
				if (this->handle_packet)
					this->handle_packet(this->user_data, this->rcv_buf.packet.payload, this->rcv_buf.packet.payload_size);

				/* Proceed to finding the next packet */
				this->state = 0;
			}
			break;
		}
	}

	return data_byte_index;
}


int packetizer_send(void *packetizer, void *data, unsigned char data_len)
{
	struct packetizer_t	*this = (struct packetizer_t *)packetizer;
	struct packet_t		packet;

	if (!this) {
		fprintf(stderr, "Attempt to send data through a non-existing packetizer\n");
		return -1;
	}

	if (!this->write_packet)
		return 0;

	/* Fill out Start of Heading and length */
	packet.soh          = SOH;
	packet.payload_size = data_len;

	/*
	 * Copy the data into the packet payload
	 * NOTE: Copying is expensive, but in this case still cheaper than calling
	 *       this->write_packet 3 times because that results in 3 expensive
	 *       system calls.
	 */
	memcpy(packet.payload, data, packet.payload_size);

	/* Send out the packet */
	return this->write_packet(this->user_data, &packet, PACKETOVERHEAD + data_len);
}


int packetizer_new(void    **packetizer, void *user_data,
		   void    (*handle_packet)(void *user_data, void *packet, unsigned char packet_len),
		   void    (*handle_oob)(void *user_data, void *data, unsigned char data_len),
		   ssize_t (*write_packet)(void *user_data, void *packet, ssize_t packet_len))
{
	struct packetizer_t	*this;
	int			result = 0;

	/* Allocate memory for this new instance */
	if (!(this = calloc(1, sizeof(struct packetizer_t)))) {
		result = -ENOMEM;
		goto err_mem;
	}

	/* Initialize non-zero data */
	this->user_data     = user_data;
	this->handle_packet = handle_packet;
	this->handle_oob    = handle_oob;
	this->write_packet  = write_packet;

	/* Fill out caller's pointer */
	*packetizer = this;

	return 0;
err_mem:
	return result;
}


void packetizer_delete(void **packetizer)
{
	struct packetizer_t	*this = (struct packetizer_t *)*packetizer;

	/* Invalidate caller's pointer first */
	*packetizer = NULL;

	if (!this) {
		fprintf(stderr, "Attempt to delete a non-existing packetizer\n");
		return;
	}

	/* Free the memory */
	free(this);
}
