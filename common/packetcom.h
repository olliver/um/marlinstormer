/*****************************************************************************/
/*** File:	packetcom.h						   ***/
/*** Date:	09-02-2015						   ***/
/*** Developer:	Robert Delien (robert@delien.nl)			   ***/
/***		Copyright (C) 2013, Clockwork Engineering		   ***/
/***									   ***/
/*** Function:	This file contains external interface for packetcom.c	   ***/
/***									   ***/
/*****************************************************************************/
#ifndef PACKETCOM_H				/* Include file already compiled? */
#define PACKETCOM_H


int packetcom_new(void    **packetcom,
		  void    *user_data,
		  void    (*handle_packet)(void *user_data, unsigned char seq_nr, unsigned char packet_type, void *data, unsigned char data_len),
		  void    (*handle_oob)(void *user_data, void *data, unsigned char data_len),
		  ssize_t (*write_packet)(void *user_data, void *packet, ssize_t packet_len));
void packetcom_delete(void **packetcom);

size_t packetcom_process(void *packetcom, void *data, size_t length);
int packetcom_send(void *packetcom, unsigned char seq_nr, void *data, size_t length);


#endif /* PACKETCOM_H */
