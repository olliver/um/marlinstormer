#ifndef HEXDUMP_H				/* Include file already compiled? */
#define HEXDUMP_H


void hexdump(FILE *stream, void *addr, size_t len);


#endif /* HEXDUMP_H */
