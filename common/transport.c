#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <limits.h>
#include <errno.h>
#include <pthread.h>
#include <sys/timerfd.h>
#include <sys/eventfd.h>

#include "transport.h"

#include "hexdump.h"
#include "inflightq.h"


/*****************************************************************************/
/*** Macros								   ***/
/*****************************************************************************/
/* Generic properties */
#define POLL_MS			10	/* TODO: Tune this */
#define MARLIN_SEQNR		255

//#define DEBUG_PACKETS
//#define DEBUG_QUEUE


/*****************************************************************************/
/*** Types								   ***/
/*****************************************************************************/
struct transport_t
{
	void			(*handle_reply)(void *user_context, uint32_t user_refnr, char *data, size_t length);
	void			(*handle_broadcast)(char *data, size_t length);
	void			(*handle_oob)(void *data, unsigned char data_len);
	ssize_t			(*write_packet)(void *packet, ssize_t packet_len);
	unsigned int		nacks;                    /* Number of NAck packets to expect */
	unsigned int		seq_nr;                   /* Sequence number for the next transmitted packet */
	int			motionqsize;              /* Number of positions in the motion queue, determined dynamically */
	int			motionqfree_count;        /* Number of free positions in the motion queue */
	int			motionqfree_fd;           /* Number of free positions in the motion queue eventfd */
	pthread_mutex_t		packet_mutex;             /* Mutex protecting packets from interrupting each other */


	/* Marlin poll timer properties */
	int			polltimer_fd;             /* File descriptor */
	unsigned char		polltimer_enabled;        /* Timer enabled state cache, preventing duplicate syscalls */
	unsigned int		polltimer_ms;	          /* Timer value in ms (We don't really need to cache this) */
	struct itimerspec  	polltimer_itval;          /* Timer value in itimerspec */

	/* Out-of-band response data */
	char			oob_response[512];
	size_t			oob_response_ndx;

	/* Subsystem instances */
	void			*inflight_queue;          /* In-flight queue instance */
	int			inflight_queue_fd;        /* Number in-flight queue free positions eventfd */
};


/* 'Command'-packet */
#define TYPE__CMD		'c'
struct packet_cmd_t {
	char			command[UCHAR_MAX];
};

/* 'Ack'-packet */
#define TYPE__ACK		'a'
struct packet_ack_t {
	uint8_t			plan_buf_free_positions;
};

/* 'NAck'-packet */
#define TYPE__NACK		'n'

/* 'Unhandled'-packet */
#define TYPE__UNHANDLED		'u'

/* 'Reply'-packet */
#define TYPE__REPLY		'r'
struct packet_reply_t {
	char			reply[255];
};

/* 'Broadcast'-packet */
#define TYPE__BROADCAST		'b'
struct packet_broadcast_t {
	char			broadcast[255];
};

/* Motion queue status update packet */
#define TYPE__QUEUE_STAT	'q'
struct packet_queue_t {
	uint8_t			plan_buf_free_positions;
};


/*****************************************************************************/
/*** Static functions							   ***/
/*****************************************************************************/
#ifdef DEBUG_PACKETS
static unsigned long timestamp_ms(void)
{
	struct timespec		timestamp;
	unsigned long		timestamp_us;

	clock_gettime(CLOCK_MONOTONIC, &timestamp);

	timestamp_us = (timestamp.tv_sec)  * 1000000 +
	               (timestamp.tv_nsec) / 1000;

	return timestamp_us / 1000;
}
#endif /* DEBUG_PACKETS */


#ifdef POLLING
static inline void polltimer_enable(struct transport_t *this, unsigned char enable)
{
	if (enable) {
		if (this->polltimer_enabled)
			return;

#ifdef DEBUG_PACKETS
		fprintf(stderr, "%ld: Enabling polling Marlin\n", timestamp_ms());
#endif /* DEBUG_PACKETS */

		if (timerfd_settime(this->polltimer_fd, 0, &this->polltimer_itval, NULL) == -1) {
			perror("Error setting poll timer");
			exit(1);
		}
	} else {
		struct itimerspec  	itval;

		if (!this->polltimer_enabled)
			return;

#ifdef DEBUG_PACKETS
		fprintf(stderr, "%ld: Disabling polling Marlin\n", timestamp_ms());
#endif /* DEBUG_PACKETS */

		memset(&itval, 0, sizeof(itval));
		if (timerfd_settime(this->polltimer_fd, 0, &itval, NULL) == -1) {
			perror("Error disabling poll timer");
			exit(1);
		}
	}
	this->polltimer_enabled = enable;
}
#endif /* POLLING */


static void poll_marlin(struct transport_t *this)
{
	char			poll_packet[] = {"M100"};

#ifdef DEBUG_PACKETS
	fprintf(stderr, "%ld: Polling Marlin (seq_nr = 0x%.2x)\n", timestamp_ms(), this->seq_nr);
#endif /* DEBUG_PACKETS */

#ifdef POLLING
	polltimer_enable(this, 0);
#endif /* POLLING */
	pthread_mutex_lock(&this->packet_mutex);
	inflightq_queue(this->inflight_queue, this->seq_nr, NO_MOTION_CMD, NULL, 0, poll_packet, strlen(poll_packet));
	if ((++this->seq_nr) >= 255)
		this->seq_nr = 0;
	pthread_mutex_unlock(&this->packet_mutex);
}


static void handle_motion_queue_update(struct transport_t *this, uint8_t plan_buf_free_positions)
{
	int			reported_free = plan_buf_free_positions;
	int			corrected_free = reported_free - inflightq_used_motions(this->inflight_queue); /* Correct the number of free motion buffer positions reported by Marlin with the number of motions still in flight */

	/* Check if the motion free eventfd needs to be adjusted */
	if (corrected_free > this->motionqfree_count) {
		int		add = corrected_free - this->motionqfree_count;

		/* Normal path: Add the difference between what is reported and what is remembered to the motion free eventfd */
#ifdef DEBUG_QUEUE
		fprintf(stderr, "Adding %d free motion positions\n", add);
#endif /* DEBUG_QUEUE */
		if (eventfd_write(this->motionqfree_fd, add) == -1) {
			perror("Error updating motion queue eventfd");
			exit(1);
		}
		this->motionqfree_count += add;

		/* Dynamically adjust the motion queue size */
		if (this->motionqfree_count > this->motionqsize) {
			fprintf(stderr, "Increasing motion queue size from %d to %d\n", this->motionqsize, this->motionqfree_count);
			this->motionqsize = this->motionqfree_count;
		}
	} else if (corrected_free < this->motionqfree_count) {
		int		subtract = this->motionqfree_count - corrected_free;

		/* Abnormal path: Subtract the difference between what is remembered and what is reported from the motion free eventfd */
		/* This situation occurres if a single motion command results into multiple motions */
#ifdef DEBUG_QUEUE
		fprintf(stderr, "Marlin reported: %d\n", plan_buf_free_positions);
		fprintf(stderr, "In flight:       %d\n", inflightq_used_motions(this->inflight_queue));
		fprintf(stderr, "Corrected:       %d\n", reported_free);
		fprintf(stderr, "Semaphore:       %d\n", this->motionqfree_count);
		fprintf(stderr, "Removing %d free motion positions\n", subtract);
#endif /* DEBUG_QUEUE */
		do {
			eventfd_t	free;

			/* Decrease the number of free motion positions by 1 */
			if (eventfd_read(this->motionqfree_fd, &free) == -1) {
				perror("Error reading in-flight queue eventfd");
				exit(1);
			}
			this->motionqfree_count--;
		} while (--subtract);
	}
#ifdef DEBUG_QUEUE
	else
		fprintf(stderr, "%d motion positions is accurate (%d reported free, %d in flight)\n", corrected_free, reported_free, inflightq_used_motions(this->inflight_queue));
#endif /* DEBUG_QUEUE */
}


static void handle_marlin(struct transport_t *this, unsigned char packet_type, void *packet_data, unsigned char data_len)
{
	struct packet_queue_t	*packet_queue;

	switch (packet_type) {
	case TYPE__QUEUE_STAT:
		if (data_len != sizeof(struct packet_queue_t))
			fprintf(stderr, "Error: Queue packet size is %d bytes (expected: %d bytes)\n", data_len, sizeof(struct packet_queue_t));
		packet_queue = (struct packet_queue_t *)packet_data;

#ifdef DEBUG_PACKETS
		fprintf(stderr, "%ld: Received Queue packet (plan_buf_free_positions = %d)\n", timestamp_ms(), packet_queue->plan_buf_free_positions);
#endif /* DEBUG_PACKETS */

		handle_motion_queue_update(this, packet_queue->plan_buf_free_positions);
		break;

	case TYPE__BROADCAST:
#ifdef DEBUG_PACKETS
		fprintf(stderr, "%ld: Received Broadcast packet\n", timestamp_ms());
#endif /* DEBUG_PACKETS */
		if (this->handle_broadcast)
			this->handle_broadcast(packet_data, data_len);
		break;

	default:
		fprintf(stderr, "Ignoring unimplemented marlin packet 0x%.2x:\n", packet_type);
		hexdump(stderr, packet_data, data_len);
		break;
	}
}


static void handle_response(struct transport_t *this, unsigned char packet_type, void *packet_data, unsigned char data_len)
{
	unsigned long		delay_us = inflightq_tailage_ms(this->inflight_queue);
	struct packet_ack_t	*packet_ack;

	/* Pass on the OOB data received so far TODO: This is better than passing on every individual byte, but there must be a better solution */
	if (this->oob_response_ndx) {
		if (this->handle_oob)
			this->handle_oob(this->oob_response, this->oob_response_ndx);
		this->oob_response_ndx = 0;
	}

	switch (packet_type) {
	case TYPE__ACK:
		if (data_len != sizeof(struct packet_ack_t))
			fprintf(stderr, "Error: 'Ack'-packet size is %d bytes (expected: %d bytes)\n", data_len, sizeof(struct packet_ack_t));
		packet_ack = (struct packet_ack_t *)packet_data;

#ifdef DEBUG_PACKETS
		fprintf(stderr, "%ld: Received 'Ack'-packet (plan_buf_free_positions = %d, delay = %ld.%.3ldms)\n", timestamp_ms(), packet_ack->plan_buf_free_positions, delay_us / 1000, delay_us % 1000);
#endif /* DEBUG_PACKETS */

		/* Check the response time */
		if (delay_us / 1000 > 20)
			if ((inflightq_tailcmd(this->inflight_queue))[0] != 'G')
				fprintf(stderr, "Slow command: '%.*s' (%ldms)\n", (int)inflightq_tailcmdlen(this->inflight_queue), inflightq_tailcmd(this->inflight_queue), delay_us / 1000);

		/* Remove the Ack'ed packet from the in-flight queue */
		inflightq_dequeue(this->inflight_queue);

		handle_motion_queue_update(this, packet_ack->plan_buf_free_positions);
		break;

	case TYPE__NACK:
		if (data_len)
			fprintf(stderr, "Error: 'NAck'-packet size is %d bytes (expected: 0 bytes)\n", data_len);

#ifdef DEBUG_PACKETS
		fprintf(stderr, "%ld: Received 'NAck'-packet (delay = %ldms)\n", timestamp_ms(), delay_us / 1000);
#endif /* DEBUG_PACKETS */

		/* If this is the first 'NAck'-packet of the series, calculate how many to expect */
		if (!this->nacks) {
			this->nacks = inflightq_used(this->inflight_queue);
#ifndef DEBUG_PACKETS
			fprintf(stderr, "Received 'NAck'-packet");
			if (this->nacks - 1)
				fprintf(stderr, ", expecting %d more\n", this->nacks - 1);
			else
				fprintf(stderr, "\n");
#endif /* DEBUG_PACKETS */
		}

		/* Resend the whole queue if we have received the last expected 'NAck'-packet */
		if (!--this->nacks) {
			inflightq_resend(this->inflight_queue);
#ifdef POLLING
			polltimer_enable(this, 0);
#endif /* POLLING */
		}

		break;

	case TYPE__UNHANDLED:
		if (data_len)
			fprintf(stderr, "Error: 'Unhandled'-packet size is %d bytes (expected: 0 bytes)\n", data_len);

#ifdef DEBUG_PACKETS
		fprintf(stderr, "%ld: Received 'Unhandled'-packet\n", timestamp_ms());
#endif /* DEBUG_PACKETS */

		fprintf(stderr, "'Unhandled'-packet received, no yet implemented, exiting\n");
		exit(1);
		break;

	case TYPE__REPLY:
#ifdef DEBUG_PACKETS
		fprintf(stderr, "%ld: Received 'Reply'-packet\n", timestamp_ms());
#endif /* DEBUG_PACKETS */

		if (this->handle_reply)
			this->handle_reply(inflightq_tail_user_context(this->inflight_queue),
			                   inflightq_tail_user_refnr(this->inflight_queue),
			                   packet_data, data_len);
		break;

	default:
		fprintf(stderr, "Ignoring unimplemented response packet 0x%.2x:\n", packet_type);
		hexdump(stderr, packet_data, data_len);
		break;
	}
}


static void handle_packet(void *user_data, unsigned char seq_nr, unsigned char packet_type, void *packet_data, unsigned char data_len)
{
	struct transport_t	*this = user_data;
	unsigned int		tail_seq_nr;

	if (seq_nr == MARLIN_SEQNR) {
		handle_marlin(this, packet_type, packet_data, data_len);
		return;
	}

	/* Check if the received packet sequence number matches that of the oldest queue enty, but not
	 * if NAcks are expected, because those have sequence number matching the originating packet */
	tail_seq_nr = inflightq_seqnr(this->inflight_queue);
	if (seq_nr != tail_seq_nr && !this->nacks) {
		fprintf(stderr, "Error: Response packet 0x%.2x nr %d does not match expected packet nr %d\n", packet_type, seq_nr, tail_seq_nr);
		exit(1);
	}

	handle_response(this, packet_type, packet_data, data_len);
}


static ssize_t packet_write(void *user_data, void *packet, ssize_t packet_len)
{
	struct transport_t	*this = user_data;

	if (this->write_packet)
		return this->write_packet(packet, packet_len);

	return 0;
}


static void oob_handler(void *user_data, void *packet_data, unsigned char data_len)
{
	struct transport_t	*this = user_data;
	char			*data = (char *)packet_data;

	if (this->oob_response_ndx < sizeof(this->oob_response)) {
		this->oob_response[this->oob_response_ndx] = *data;
		this->oob_response_ndx++;
	}
}


/*****************************************************************************/
/*** Functions								   ***/
/*****************************************************************************/
void transport_get_queueinfo(void *transport, unsigned char *inflight_size, unsigned char *inflight_used, unsigned char *motion_size, unsigned char *motion_used)
{
	struct transport_t	*this = (struct transport_t *)transport;

	*inflight_size = inflightq_size(this->inflight_queue);;
	*inflight_used = inflightq_used(this->inflight_queue);;
	*motion_size = this->motionqsize;
	*motion_used = this->motionqsize - this->motionqfree_count;
}


void transport_send(void *transport, unsigned char is_motion, void *user_context, uint32_t user_refnr, void *data, size_t length)
{
	struct transport_t	*this = (struct transport_t *)transport;

#ifdef POLLING
	polltimer_enable(this, 0);
#endif /* POLLING */

	if (is_motion) {
		eventfd_t		free;

		/* Decrease the number of free motion positions by 1 */
		if (eventfd_read(this->motionqfree_fd, &free) == -1) {
			perror("Error reading in-flight queue eventfd");
			exit(1);
		}
		this->motionqfree_count--;
	}

	pthread_mutex_lock(&this->packet_mutex);
	inflightq_queue(this->inflight_queue, this->seq_nr, is_motion, user_context, user_refnr, data, length);
	if ((++this->seq_nr) >= 255)
		this->seq_nr = 0;
	pthread_mutex_unlock(&this->packet_mutex);
}


size_t transport_process(void *transport, void *data, size_t length)
{
	struct transport_t	*this = (struct transport_t *)transport;

	return inflightq_process(this->inflight_queue, data, length);
}


int transport_motionq_fd(void *transport)
{
	struct transport_t	*this = (struct transport_t *)transport;

	return this->motionqfree_fd;
}


int transport_inflightq_fd(void *transport)
{
	struct transport_t	*this = (struct transport_t *)transport;

	return this->inflight_queue_fd;
}


int transport_worker(void *transport)
{
	struct transport_t	*this = (struct transport_t *)transport;
	uint64_t		missed;
	ssize_t			bytes;

	/* Re-set the timer */
	if ((bytes = read(this->polltimer_fd, &missed, sizeof(missed))) == -1) {
		perror("Error reading poll timer");
		return -errno;
	}

	/* Poll marlin */
	poll_marlin(this);

	return 0;
}


int transport_new(void    **transport,
		  void    (*handle_reply)(void *user_context, uint32_t user_refnr, char *data, size_t length),
		  void    (*handle_broadcast)(char *data, size_t length),
		  void    (*handle_oob)(void *data, unsigned char data_len),
		  ssize_t (*write_packet)(void *packet, ssize_t packet_len))
{
	struct transport_t	*this;
	int			result = 0;

	/* Allocate memory for this new instance */
	if (!(this = calloc(1, sizeof(struct transport_t)))) {
		result = -ENOMEM;
		goto err_mem;
	}

	/* Initialize non-zero data */
	this->handle_reply     = handle_reply;
	this->handle_broadcast = handle_broadcast;
	this->handle_oob       = handle_oob;
	this->write_packet     = write_packet;

	/* Create an eventfd for the motion queue usage */
	if ((this->motionqfree_fd = eventfd(this->motionqsize, EFD_SEMAPHORE)) == -1) {
		perror("Error motion queue eventfd");
		result = -errno;
		goto err_motionefd;
	}
	this->motionqfree_count = this->motionqsize;  /* Is still 0 here; Will be adjusted dynamically */

	/* Create an in-flight queue instance */
	if ((this->inflight_queue_fd = inflightq_new(&this->inflight_queue, this, handle_packet, oob_handler, packet_write)) < 0) {
		result = this->inflight_queue_fd;
		fprintf(stderr, "Error %d creating in-flight queue instance", result);
		goto err_inflightq;
	}

	if ((result = pthread_mutex_init(&this->packet_mutex, NULL))) {
		fprintf(stderr, "Error %d initializing packet mutex", result);
		goto err_packet_mutex;
	}

	/* Create Marlin poll timer */
	if ((this->polltimer_fd = timerfd_create(CLOCK_MONOTONIC, 0)) == -1) {
		perror("Error creating poll timer");
		result = -errno;
		goto err_timer;
	}
	memset(&this->polltimer_itval, 0, sizeof(this->polltimer_itval));
	this->polltimer_itval.it_value.tv_sec  = this->polltimer_ms / 1000;
	this->polltimer_itval.it_value.tv_nsec = (this->polltimer_ms % 1000) * 1000000UL;

	/* Fill out caller's pointer */
	*transport = this;

	return this->polltimer_fd;
	close(this->polltimer_fd);
err_timer:
	pthread_mutex_destroy(&this->packet_mutex);
err_packet_mutex:
	inflightq_delete(&this->inflight_queue);
err_inflightq:
	close(this->motionqfree_fd);
err_motionefd:
	free(this);	
err_mem:
	return result;
}


void transport_delete(void **transport)
{
	struct transport_t	*this = (struct transport_t *)*transport;

	/* Invalidate caller's pointer first */
	*transport = NULL;

	if (!this) {
		fprintf(stderr, "Attempt to delete a non-existing transport\n");
		return;
	}

	close(this->polltimer_fd);
	pthread_mutex_destroy(&this->packet_mutex);
	inflightq_delete(&this->inflight_queue);
	close(this->motionqfree_fd);

	/* Free the memory */
	free(this);
}
