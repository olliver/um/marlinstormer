#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/eventfd.h>

#include "packetcom.h"


/*****************************************************************************/
/*** Macros								   ***/
/*****************************************************************************/
/* Generic properties */
#define MAX_INFLIGHT		5
#define MAX_GCODE_LEN		100

/* Macro to calculate the number of used entries in the queue */
#define QUEUE_USED(h, t)	(((h)>=(t))?((h)-(t)):((h)+((MAX_INFLIGHT)+1)-(t)))

//#define DEBUG


/*****************************************************************************/
/*** Types								   ***/
/*****************************************************************************/
struct entry_t
{
	unsigned int		send_counter;             /* Number of times this entry was sent */
	struct timespec		send_ts;                  /* Time stamp at which this entry was last sent */
	unsigned int		seq_nr;                   /* Sequence number used to send this enty */
	unsigned char		is_motion;                /* Marks whether or not this is a motion command */
	void			*user_context;            /* Context pointer for this command given by user */
	uint32_t		user_refnr;               /* Reference number for this command given by user */
	size_t			length;                   /* Data length */
	unsigned char		data[MAX_GCODE_LEN];      /* Data */
};

struct queue_t
{
	void			*user_data;               /* Reference for layer using this module */
	void			*packetcom;               /* Packet encoder/decoder instance */
	unsigned int		head;                     /* Index to the entry next up for transmission */
	unsigned int		tail;                     /* Index to the entry expeded a reply from */
	int			free_fd;                  /* Number of free entries eventfd */
	unsigned int		motions;                  /* Number of motion commands in the queue */
	struct entry_t		entry[MAX_INFLIGHT + 1];  /* Queue entries */
};


/*****************************************************************************/
/*** Static functions							   ***/
/*****************************************************************************/


/*****************************************************************************/
/*** Functions								   ***/
/*****************************************************************************/
size_t inflightq_process(void *inflightq, void *data, size_t length)
{
	struct queue_t		*this = (struct queue_t *)inflightq;

	return packetcom_process(this->packetcom, data, length);
}

int inflightq_size(void *inflightq)
{
	return MAX_INFLIGHT;
}

int inflightq_used(void *inflightq)
{
	struct queue_t		*this = (struct queue_t *)inflightq;

	return QUEUE_USED(this->head, this->tail);
}

int inflightq_used_motions(void *inflightq)
{
	struct queue_t		*this = (struct queue_t *)inflightq;

	return this->motions;
}


unsigned char inflightq_seqnr(void *inflightq)
{
	struct queue_t		*this = (struct queue_t *)inflightq;
	struct entry_t		*tail = &this->entry[this->tail];

	return tail->seq_nr;
}

void *inflightq_tail_user_context(void *inflightq)
{
	struct queue_t		*this = (struct queue_t *)inflightq;
	struct entry_t		*tail = &this->entry[this->tail];

	return tail->user_context;
}

uint32_t inflightq_tail_user_refnr(void *inflightq)
{
	struct queue_t		*this = (struct queue_t *)inflightq;
	struct entry_t		*tail = &this->entry[this->tail];

	return tail->user_refnr;
}

unsigned long inflightq_tailage_ms(void *inflightq)
{
	struct queue_t		*this = (struct queue_t *)inflightq;
	struct entry_t		*tail = &this->entry[this->tail];
	struct timespec		ts;

	/* Get the arrival time and calculate the reply delay */
	if (clock_gettime(CLOCK_MONOTONIC, &ts) == -1)
		return -1;

	return (ts.tv_sec  - tail->send_ts.tv_sec)  * 1000000 +
	       (ts.tv_nsec - tail->send_ts.tv_nsec) / 1000;
}

unsigned char *inflightq_tailcmd(void *inflightq)
{
	struct queue_t		*this = (struct queue_t *)inflightq;
	struct entry_t		*tail = &this->entry[this->tail];

	return tail->data;
}

size_t inflightq_tailcmdlen(void *inflightq)
{
	struct queue_t		*this = (struct queue_t *)inflightq;
	struct entry_t		*tail = &this->entry[this->tail];

	return tail->length;
}

void inflightq_queue(void *inflightq, unsigned char seq_nr, unsigned char is_motion, void *user_context, uint32_t user_refnr, void *data, size_t length)
{
	struct queue_t		*this = (struct queue_t *)inflightq;
	struct entry_t		*head = &this->entry[this->head];
	eventfd_t		free;

	if (length > MAX_GCODE_LEN) {
		fprintf(stderr, "Error: Packet length %d too long\n", (int)length);
		return;
	}

	/* Decrease the number of free positions by 1 */
	if (eventfd_read(this->free_fd, &free) == -1) {
		perror("Error reading in-flight queue eventfd");
		exit(1);
	}

	/* Copy the gcode into the in-flight queue */
	head->send_counter = 0;
	head->seq_nr       = seq_nr;
	head->is_motion    = is_motion;
	head->user_context = user_context;
	head->user_refnr   = user_refnr;
	head->length       = length;
	memcpy(head->data, data, length);

	/* Add the packet to the queue */
	if (++this->head > MAX_INFLIGHT)
		this->head = 0;
	if (head->is_motion)
		this->motions++;

	/* Protect against queue over-flows */
	if (this->head == this->tail) {
		fprintf(stderr, "Error: Queue overflow!\n");
		exit(1);
	}

	/* Send the packet immediately */
	packetcom_send(this->packetcom, (unsigned char)head->seq_nr, head->data, head->length);

	/* Update the status */
	clock_gettime(CLOCK_MONOTONIC, &head->send_ts);
	head->send_counter++;
}


void inflightq_resend(void *inflightq)
{
	struct queue_t		*this = (struct queue_t *)inflightq;
	unsigned int		tail = this->tail;
	unsigned int		rejects = QUEUE_USED(this->head, this->tail);
#ifdef DEBUG
	unsigned int		resent = 1;
#endif /* DEBUG */

#ifndef DEBUG
	fprintf(stderr, "Resending %d packet%s\n", rejects, (rejects != 1)?"s":"");
#endif /* DEBUG */

	while (tail != this->head) {
		struct entry_t		*entry = &this->entry[tail];

#ifdef DEBUG
		fprintf(stderr, "Resending %d/%d (seq_nr = 0x%.2x)\n", resent++, rejects, entry->seq_nr);
#endif /* DEBUG */

		/* Send the packet immediately */
		packetcom_send(this->packetcom, (unsigned char)entry->seq_nr, entry->data, entry->length);

		/* Update the status */
		clock_gettime(CLOCK_MONOTONIC, &entry->send_ts);
		entry->send_counter++;

		if (++tail > MAX_INFLIGHT)
			tail = 0;
	}
}


void inflightq_dequeue(void *inflightq)
{
	struct queue_t		*this = (struct queue_t *)inflightq;
	struct entry_t		*tail = &this->entry[this->tail];

	/* Remove the packet from the queue */
	if (tail->is_motion)
		this->motions--;
	if (++this->tail > MAX_INFLIGHT)
		this->tail = 0;

	/* Increase the number of free positions by 1 */
	if (eventfd_write(this->free_fd, 1) == -1) {
		perror("Error writing in-flight queue eventfd");
		exit(1);
	}
}


int inflightq_new(void    **inflightq, void *user_data,
		  void    (*handle_packet)(void *user_data, unsigned char seq_nr, unsigned char packet_type, void *data, unsigned char data_len),
		  void    (*handle_oob)(void *user_data, void *data, unsigned char data_len),
		  ssize_t (*write_packet)(void *user_data, void *packet, ssize_t packet_len))
{
	struct queue_t		*this;
	int			result = 0;

	/* Allocate memory for this new instance */
	if (!(this = calloc(1, sizeof(struct queue_t)))) {
		result = -ENOMEM;
		goto err_mem;
	}

	/* Initialize non-zero data */
	this->user_data = user_data;

	/* Create an eventfd for the number of free entries */
	if ((this->free_fd = eventfd(MAX_INFLIGHT, EFD_SEMAPHORE)) == -1) {
		perror("Error in-flight queue free eventfd");
		result = -errno;
		goto err_eventfd;
	}

	/* Create a packet encoder/decoder instance and let it call provided function vectors directly */
	if ((result = packetcom_new(&this->packetcom, user_data, handle_packet, handle_oob, write_packet)) < 0) {
		fprintf(stderr, "Error %d creating packetcom instance", result);
		goto err_packetcom;
	}

	/* Fill out caller's pointer */
	*inflightq = this;

	return this->free_fd;
	packetcom_delete(&this->packetcom);
err_packetcom:
	close(this->free_fd);
err_eventfd:
	free(this);	
err_mem:
	return result;
}


void inflightq_delete(void **inflightq)
{
	struct queue_t		*this = (struct queue_t *)*inflightq;

	/* Invalidate caller's pointer first */
	*inflightq = NULL;

	if (!this) {
		fprintf(stderr, "Attempt to delete a non-existing inflightq\n");
		return;
	}

	packetcom_delete(&this->packetcom);

	close(this->free_fd);

	/* Free the memory */
	free(this);
}
