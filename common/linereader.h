#ifndef LINEREADER_H				/* Include file already compiled? */
#define LINEREADER_H


int linereader_new(void    **linereader, char *file_name);
void linereader_delete(void **linereader);
int linereader_read(void *linereader, void *buf, size_t size);


#endif /* LINEREADER_H */

