#ifndef PACKETIZER_H				/* Include file already compiled? */
#define PACKETIZER_H


size_t packetizer_process(void *packetizer, void *data, size_t length);
int packetizer_send(void *packetizer, void *data, unsigned char data_len);

int packetizer_new(void    **packetizer,
		   void    *user_data,
		   void    (*handle_packet)(void *user_data, void *packet, unsigned char packet_len),
		   void    (*handle_oob)(void *user_data, void *data, unsigned char data_len),
		   ssize_t (*write_packet)(void *user_data, void *packet, ssize_t packet_len));
void packetizer_delete(void **packetizer);


#endif /* PACKETIZER_H */
