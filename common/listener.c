#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <poll.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <errno.h>
#include <pthread.h>
#include <netinet/tcp.h>
#include <sys/timerfd.h>
#include <sys/ioctl.h>

#include "hexdump.h"
#include "listener.h"


/*****************************************************************************/
/*** Macros								   ***/
/*****************************************************************************/
#define ARRAY_SIZE(arr)		(sizeof(arr) / sizeof((arr)[0]))


/*****************************************************************************/
/*** Types								   ***/
/*****************************************************************************/
struct listener_t {
	/* Properties initialized only once */
	short			port_nr;				/* Port number */
	void			*user_data;				/* User_data provided to data_handler function */
	int			(*conn_handler)(void *user_data, char *client_ip, int client_fd); /* Call-back function for connection change */

	pthread_t		pthread;				/* Pthread to handle incoming connections */
	int			pipe[2];				/* Pipe to communicate with pthread */
	int			ssock;					/* Listening socket */
};


/*****************************************************************************/
/*** Static functions							   ***/
/*****************************************************************************/
static void *listener_thread(void *arg)
{
	struct listener_t	*this = (struct listener_t *)arg;
	struct pollfd		fds[2];

	/* Setup fd array for poll */
	memset(fds, 0 , sizeof(fds));
	fds[0].fd     = this->pipe[0];
	fds[0].events = POLLIN;
	fds[1].fd     = this->ssock;
	fds[1].events = POLLIN;

	/* Listen for new connections */
	for (;;) {
		/* Wait for data or pipe */
		if (poll(fds, ARRAY_SIZE(fds), -1) < 0) {
			perror("Error polling listener");
			break;
		}

		/* Check if we have any messages */
		if (fds[0].revents & POLLIN) {
			ssize_t		bytes;
			char		message[1];

			if ((bytes = read(this->pipe[0], message, sizeof(message))) == -1) {
				perror("Error reading listener message pipe");
				break;
			}
			/* Check if we are told to quit */
			if (message[bytes-1] == 'Q') {
				fprintf(stderr, "Quitting listener thread for port %hu\n", this->port_nr);
				break;
			}
			fprintf(stderr, "Warning: Received unsupported message 0x%.2x through listener message pipe\n", message[bytes-1]);
		}

		if (fds[1].revents & POLLHUP) {
			fprintf(stderr, "Error POLLHUP listener accept\n");
			break;
		}
		if (fds[1].revents & POLLERR) {
			fprintf(stderr, "Error POLLERR listener accept\n");
			break;
		}
		if (fds[1].revents & POLLNVAL) {
			fprintf(stderr, "Error POLLNVAL listener accept\n");
			break;
		}

		if (fds[1].revents & POLLIN) {
			struct sockaddr_in	client_addr;
			socklen_t		clen = sizeof(client_addr);
			int			flags;
			char			client_ip[INET_ADDRSTRLEN];
			int			csock;

			/* Accept the incoming connection */
			if ((csock = accept(this->ssock, (struct sockaddr *)&client_addr, &clen)) < 0) {
				perror("Error accepting incoming listener connection");
				continue;
			}

			/* Make client socket fd non-blocking, to prevent the reader thread to be blocked by fwrite */
			if ((flags = fcntl(csock, F_GETFL, 0)) < 0) {
				perror("Error getting client socket flags");
				close(csock);
				continue;
			}
			flags |= O_NONBLOCK;
			fcntl(csock, F_SETFL, flags);

			/* Fetch the client IP address */
			inet_ntop(AF_INET, &client_addr.sin_addr.s_addr, client_ip, INET_ADDRSTRLEN);

			/* Call the connection handler */
			if (!this->conn_handler ||
			    this->conn_handler(this->user_data, client_ip, csock) < 0) {
				fprintf(stderr, "Error handling new connection\n");
				close(csock);
				continue;
			}
		}
	}

	fprintf(stderr, "Listener thread for port %hu exited\n", this->port_nr);
	return 0;
}


/*****************************************************************************/
/*** Functions								   ***/
/*****************************************************************************/
int listener_new(void **listener, void *user_data, short port_nr,
		 int (*conn_handler)(void *user_data, char *client_ip, int client_fd))
{
	struct listener_t	*this;
	struct sockaddr_in	listener_addr;
	const int		yes = 1;
	int			flags;
  	int			result = 0;

	/* Allocate memory for this new listener */
	if (!(this = malloc(sizeof(struct listener_t)))) {
		result = -ENOMEM;
		fprintf(stderr, "Error %d allocating memory for listener\n", result);
		goto err_mem;
	}
	memset(this, 0, sizeof(struct listener_t));

	/* Initialize new listener data */
	this->user_data    = user_data;
	this->port_nr      = port_nr;
	this->conn_handler = conn_handler;

	/* Create a TCP socket */
	if ((this->ssock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		perror("Error opening listener socket");
		result = -errno;
		goto err_ssock;
	}

	/* Configure the socket */
	memset(&listener_addr, 0, sizeof(listener_addr));
	listener_addr.sin_family      = AF_INET;
	listener_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	listener_addr.sin_port        = htons(this->port_nr);

	/* Set socket options */
	if (setsockopt(this->ssock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) < 0) {
		perror("Error setting listener socket address reuse");
		result = -errno;
		goto err_reuseaddr;
	}
	if (setsockopt(this->ssock, SOL_SOCKET, SO_KEEPALIVE, &yes, sizeof(yes)) < 0) {
		perror("Error activating listener socket keep-alive");
		result = -errno;
		goto err_keepalive;
	}

	/* Make listener socket fd non-blocking, to prevent race condition due to blocking accept when drops connection attempt between two syscalls */
	if ((flags = fcntl(this->ssock, F_GETFL, 0)) < 0) {
		perror("Error getting listener socket flags");
		result = -errno;
		goto err_nonblock;
	}
	flags |= O_NONBLOCK;
	fcntl(this->ssock, F_SETFL, flags);

	/* Bind the socket */
	if (bind(this->ssock, (struct sockaddr*)&listener_addr, sizeof(listener_addr)) < 0) {
		perror("Error binding listener socket");
		result = -errno;
		goto err_bind;
	}

	/* Make socket listening */
	if (listen(this->ssock, 1) < 0) {
		perror("Error making listener socket listening");
		result = -errno;
		goto err_listen;
	}

	/* Create a communications pipe for the thread */
	if (pipe(this->pipe) < 0) {
		perror("Error initializing listener pipe");
		result = -errno;
		goto err_pipe;
	}

	/* Fill out caller's pointer */
	*listener = this;

	/* Create a listener thread */
	if ((result = pthread_create(&this->pthread, NULL, &listener_thread, this)) < 0) {
		fprintf(stderr, "Error %d creating listener thread\n", result);
		goto err_thread;
	}

	return 0;
err_thread:
	*listener = NULL;
	close(this->pipe[1]);
	close(this->pipe[0]);
err_pipe:
err_listen:
err_bind:
err_nonblock:
err_keepalive:
err_reuseaddr:
	shutdown(this->ssock, SHUT_RDWR);
	close(this->ssock);
err_ssock:
	free(this);
err_mem:
	return result;
}


void listener_delete(void **listener)
{
	struct listener_t	*this = (struct listener_t *)*listener;
	ssize_t			bytes;

	/* Invalidate caller's pointer first */
	*listener = NULL;

	if (!this) {
		fprintf(stderr, "Attempt to delete a non-existing listener\n");
		return;
	}

	/* Tell pthread to quit */
	if ((bytes = write(this->pipe[1], "Q", 1)) <= 0) {
		perror("Error stopping listener pthread, canceling it instead");
		pthread_cancel(this->pthread);

	} else
		/* Wait for the pthread to finish */
		pthread_join(this->pthread, NULL);

	/* Teardown */
	close(this->pipe[1]);
	close(this->pipe[0]);
	shutdown(this->ssock, SHUT_RDWR);
	close(this->ssock);
	free(this);
}
