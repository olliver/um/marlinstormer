/******************************************************************************/
/* File    :	crc8.h							      */
/* Function:	Include file of 'crc8.c'.				      */
/* Author  :	Robert Delien						      */
/*		Copyright (C) 2013, Clockwork Engineering		      */
/******************************************************************************/
#ifndef CRC8_H					/* Include file already compiled? */
#define CRC8_H


/* Generic */
uint8_t		crc8_calculate		(void		*data,
					 size_t		length);
int		crc8_verify		(void		*data,
					 size_t		length,
					 uint8_t	crc8);


#endif /* CRC8_H */
