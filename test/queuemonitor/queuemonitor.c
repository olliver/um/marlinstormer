#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <limits.h>
#include <linux/serial.h>
#include <poll.h>
#include <errno.h>
#include <stropts.h>
#include <asm/ioctls.h>
#include <libgen.h>
#include <sys/timerfd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <setjmp.h>
#include <execinfo.h>
#include <stddef.h>

#include "packetizer.h"


/*****************************************************************************/
/*** Macros								   ***/
/*****************************************************************************/
/* TCP port to send packets to */
#define SERVER_TCP_PORT		1979
#define MAX_GCODE_LEN		100

/* Macro to calculate the number of members in an array */
#define ARRAY_SIZE(arr)		(sizeof(arr) / sizeof((arr)[0]))

//#define DEBUG


/*****************************************************************************/
/*** Types								   ***/
/*****************************************************************************/
struct tcp_command_t
{
	unsigned char	type;
	unsigned char	cmd[UCHAR_MAX - sizeof(unsigned char)];
} __attribute__((packed));

struct packet_motion_command_t {
	unsigned char	data[sizeof(((struct tcp_command_t *)0)->cmd)];
} __attribute__((packed));

struct packet_command_t {
	uint32_t	refnr;
	unsigned char	data[sizeof(((struct tcp_command_t *)0)->cmd) - sizeof(unsigned char)];
} __attribute__((packed));

struct packet_reply_t {
	uint32_t	refnr;
	unsigned char	data[sizeof(((struct tcp_command_t *)0)->cmd) - sizeof(unsigned char)];
} __attribute__((packed));

struct packet_queue_request_t {
	uint32_t	refnr;
} __attribute__((packed));

struct packet_queue_reply_t {
	uint32_t	refnr;
	unsigned char	inflight_size;
	unsigned char	inflight_used;
	unsigned char	motion_size;
	unsigned char	motion_used;
} __attribute__((packed));


/*****************************************************************************/
/*** Globals								   ***/
/*****************************************************************************/
/* Command line options */
static int			verbosity = 0;            /* Verbosity level */

/* Back-trace data */
static void			*bt_trace[20];            /* Back-trace buffer */
static size_t			bt_count;                 /* Back-trace count */
static jmp_buf			bt_env;                   /* Back-trace environment */

/* Server address */
static struct hostent		*server;                  /* Server IP address */
static short			port;                     /* Server TCP port number */

static int			timer_fd;
static struct pollfd		fds[2];                   /* File descriptors to poll in the main loop */

/*  */
static void			*packetizer;

/* Client socket data */
static int			sock;                     /* Network socket to server */


/*****************************************************************************/
/*** Static functions							   ***/
/*****************************************************************************/
#ifdef DEBUG
static unsigned long timestamp_ms(void)
{
	struct timespec		timestamp;
	unsigned long		timestamp_us;

	clock_gettime(CLOCK_MONOTONIC, &timestamp);

	timestamp_us = (timestamp.tv_sec)  * 1000000 +
	               (timestamp.tv_nsec) / 1000;

	return timestamp_us / 1000;
}
#endif /* DEBUG */


static int send_marlin_command(char *data)
{
	static uint32_t		refnr = 0;
	size_t			len = strlen(data);
	struct tcp_command_t	tcp_command = {
		.type = 'C'
	};
	struct packet_command_t	*packet_command = (struct packet_command_t *)&tcp_command.cmd;

	if (len > sizeof(tcp_command.cmd)) {
		fprintf(stderr, "Error sending %u byte marlin command (%u max.)\n", (unsigned int)len, (unsigned int)sizeof(tcp_command.cmd));
		return -1;
	}

	memcpy(&packet_command->refnr, &refnr, sizeof(packet_command->refnr));
	memcpy(packet_command->data, data, len);

	refnr++;

	return packetizer_send(packetizer, &tcp_command, sizeof(tcp_command.type) + sizeof(packet_command->refnr) + len);
}


static int send_queue_command(void)
{
	static uint32_t		refnr = 0;
	struct tcp_command_t	tcp_command = {
		.type = 'Q'
	};
	struct packet_queue_request_t	*packet_queue_request = (struct packet_queue_request_t *)&tcp_command.cmd;

#ifdef DEBUG
	fprintf(stderr, "%ld: Sending queue command refnr. 0x%.8x'\n", timestamp_ms(), refnr);
#endif /* DEBUG */

	memcpy(&packet_queue_request->refnr, &refnr, sizeof(packet_queue_request->refnr));

	refnr++;

	return packetizer_send(packetizer, &tcp_command, sizeof(tcp_command.type) + sizeof(struct packet_queue_request_t));
}


static void handle_marlin_broadcast(void *data, unsigned char len)
{
#ifdef DEBUG
	fprintf(stderr, "%ld: Received broadcast '%.*s'\n", timestamp_ms(), len, (char *)data);
#endif /* DEBUG */
}


static void handle_marlin_reply(void *data, unsigned char len)
{
	uint32_t		refnr = 0;
	struct packet_reply_t	*packet_reply = (struct packet_reply_t *)data;

	memcpy(&refnr, &packet_reply->refnr, sizeof(refnr));

#ifdef DEBUG
	fprintf(stderr, "%ld: Received reply with refnr. 0x%.8x '%.*s'\n", timestamp_ms(), refnr, len - (int)sizeof(packet_reply->refnr), (char *)&packet_reply->data);
#endif /* DEBUG */
}


static void handle_queue_reply(void *data, unsigned char len)
{
	struct packet_queue_reply_t	*packet_queue_reply = (struct packet_queue_reply_t *)data;
	uint32_t			refnr;

	/* Memcpy: Source may be unaligned */
	memcpy(&refnr, &packet_queue_reply->refnr, sizeof(refnr));

#ifdef DEBUG
	fprintf(stderr, "%ld: Received queue reply: refnr = 0x%.8x, inflight_size = %d, inflight_used = %d, motion_size = %d, motion_used = %d\n", timestamp_ms(),
	                refnr, packet_queue_reply->inflight_size, packet_queue_reply->inflight_used, packet_queue_reply->motion_size, packet_queue_reply->motion_used);
#else
	fprintf(stderr, "inflight_size = %d, inflight_used = %d, motion_size = %d, motion_used = %d\n",
	                packet_queue_reply->inflight_size, packet_queue_reply->inflight_used, packet_queue_reply->motion_size, packet_queue_reply->motion_used);
#endif /* DEBUG */
}


static void handle_tcp_packet(void *user_data, void *packet, unsigned char packet_len)
{
	/* Command handler table */
	static struct handler_table_t {
		unsigned char		type;
		void			(*handler)(void *data, unsigned char len);
	} handler_table[] = {
		{.type = 'B', .handler = handle_marlin_broadcast},
		{.type = 'R', .handler = handle_marlin_reply},
		{.type = 'Q', .handler = handle_queue_reply}
	};

	struct tcp_command_t	*tcp_command = (struct tcp_command_t *)packet;
	int			table_ndx = 0;

	while (table_ndx < ARRAY_SIZE(handler_table)) {
		if (tcp_command->type == handler_table[table_ndx].type && handler_table[table_ndx].handler) {
			handler_table[table_ndx].handler(tcp_command->cmd, packet_len - offsetof(struct tcp_command_t, cmd));
			return;
		}
		table_ndx++;
	}
	fprintf(stderr, "Error handling unsupported %.2x packet (%d bytes)\n", tcp_command->type, packet_len - (int)sizeof(tcp_command->type));
}


static ssize_t write_tcp_packet(void *user_data, void *packet, ssize_t packet_len)
{
	return write(sock, packet, packet_len);
}


static ssize_t read_data_from_server(void)
{
	ssize_t		bytes;
	unsigned char	buffer[512];

	bytes = read(sock, buffer, sizeof(buffer));
	if (bytes > 0)
		packetizer_process(packetizer, buffer, bytes);
	else if (!bytes) {
		fprintf(stderr, "Connection closed\n");
		exit(1);
	} else {
		perror("Error reading network socket");
		exit(1);
	}
	return bytes;
}


/*****************************************************************************/
/*** Utilitarian static functions					   ***/
/*****************************************************************************/
static int init(void)
{
	struct sockaddr_in	server_addr;
	struct itimerspec	itval;
	int			result = 0;

	/* Create a TCP socket */
	if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		perror("Error opening socket");
		result = -errno;
		goto err_socket;
	}

	/* Configure the socket */
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port   = htons(port);
	memcpy(&server_addr.sin_addr.s_addr, server->h_addr, server->h_length);

	/* Create a packetizer */
	if ((result = packetizer_new(&packetizer, NULL, handle_tcp_packet, NULL, write_tcp_packet)) < 0) {
		fprintf(stderr, "Error %d creating packetizer\n", result);
		goto err_packetizer;
	}

	/* Connect to server */
	if ((result = connect(sock, (struct sockaddr *)&server_addr, sizeof(server_addr))) < 0) {
		perror("Error connecting");
		result = -errno;
		goto err_connect;
	}

	/* Create a timer to measure performance */
        if ((timer_fd = timerfd_create(CLOCK_MONOTONIC, 0)) < 0) {
		perror("Error creating performance timer");
                result = -errno;
                goto err_timer;
        }
        /* Make performance timer periodic */
        memset(&itval, 0, sizeof(itval));
        itval.it_interval.tv_sec  = 1;
        itval.it_value.tv_sec     = 1;
        if ((result = timerfd_settime(timer_fd, 0, &itval, NULL)) < 0) {
		perror("Error setting performance timer");
                result = -errno;
                goto err_set_timer;
        }

	return 0;
err_set_timer:
	close(timer_fd);
err_timer:
err_connect:
	packetizer_delete(&packetizer);
err_packetizer:
	close(sock);
err_socket:
	return result;
}


static void term(void)
{
	close(timer_fd);
	packetizer_delete(&packetizer);
	close(sock);
}


static void exit_handler(int ev, void *arg)
{
	term();
}


/*
 * This function installed as a signal handler, and is called automatically
 * whenever a SIGINT, SIGQUIT, SIGTERM or SIGSEGV is received. Its purpose is
 * to exit normally, enabling the terminator to shut down all subsystems and to
 * pull Marlin into reset.
 * Handling of SIGSEGV is a special one. Because printing is not recommended
 * within a signal handler, the back-trace is stored and a long jump to main
 * is made, so it can be printed in main context.
 */
static void signal_handler(int signum)
{
	switch (signum) {
	case SIGINT:
	case SIGQUIT:
	case SIGTERM:
		/* Exit gracefully */
		exit(signum);
		break;
	case SIGSEGV:
		/* Save the trace in statics */
		bt_count = backtrace(bt_trace, ARRAY_SIZE(bt_trace));

		/* Jump to main context */
		longjmp(bt_env, signum);
	}
}


/*****************************************************************************/
/*** Functions								   ***/
/*****************************************************************************/
int main(int argc, char* argv[])
{
	int			arg;
	int			signum;
	int			result = 0;

	/* Install signal handler */
	signal(SIGINT,  signal_handler);
	signal(SIGQUIT, signal_handler);
	signal(SIGTERM, signal_handler);
	signal(SIGSEGV, signal_handler);
	signal(SIGPIPE, SIG_IGN); /* Ignore SIGPIPE */

	/* Create an environment copy to allow jumping here from signal handler, decoupling calls to syslog, sync et all */
	if ((signum = setjmp(bt_env)) != 0) {
		char		**strings = backtrace_symbols(bt_trace, bt_count);
		size_t		index;

		fprintf(stderr, "Caught signal %d\n", signum);
		for (index = 0; index < bt_count; index++)
			fprintf(stderr, "Back-trace: %s\n", strings[index]);
		exit(EXIT_FAILURE);
	}

	/* Initialize non-zero defaults */
	server   = gethostbyname("localhost");
	port     = SERVER_TCP_PORT;

	/* Process the command line arguments */
	while ((arg = getopt(argc, argv, "p:s:vh")) != -1) {
		switch (arg) {
		case 'p':
			/* Retrieve server TCP port */
			port = strtol(optarg, NULL, 10);
			break;
		case 's':
			/* Retrieve server IP address */
			server = gethostbyname(optarg);
			break;
		case 'v':
			/* Be verbose */
			verbosity++;
			break;
		default:
			result = -1;
		case 'h':
			fprintf(stderr, "Usage: %s\n", basename(argv[0]));
			fprintf(stderr, "-v                 Be verbose\n");
			fprintf(stderr, "-h                 Show this help\n");
			return result;
		}
	}
	argc -= optind;
	argv += optind;

	if (argc) {
		fprintf(stderr, "Error: Positional arguments are not supported\n");
		exit(-1);
	}

	if (init() < 0)
		exit(-1);
	on_exit(exit_handler, NULL);

	memset(&fds, 0, sizeof(fds));
	fds[0].fd      = sock;
	fds[0].events |= POLLIN;
	fds[1].fd      = timer_fd;
	fds[1].events |= POLLIN;

	for (;;) {
		if (poll(fds, ARRAY_SIZE(fds), -1) == -1) {
			perror("Error polling socket");
			result = -errno;
			break;
		}

		if (fds[0].revents & POLLIN)
			read_data_from_server();

		if (fds[1].revents & POLLIN) {
			unsigned long long	missed;

			if (read(timer_fd, &missed, sizeof(missed)) < 0) {
				perror("Error reading timer");
				result = -errno;
				break;
			}

			send_queue_command();
		}
	}

	return result;
}
