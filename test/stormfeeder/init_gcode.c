const char  init_s5_gcode[][50] = {
	/* G-Codes to setup Marlin */
	"M999",
	"M115",
	"M80",
	"M405 S1",
	"M149 T0",
	"M12000 X340",
	"M12000 Y247.5",
	"M12000 Z315",
	"M12001 X-27.8",
	"M12001 Y0",
	"M150 T0 P0",
	"M12001 Z0",
	"M405 S0",
	"M220 S100",
	"M221 S100",
	"M204 S3000",
	"M150 T0 P1",
	"M204 T3000",
	"M203 X300",
	"M203 Y300",
	"M203 Z40",
	"M203 E45",
	"M150 T0 P2",
	"M205 X20",
	"M205 Z0.4",
	"M205 E5",
	"M907 X1300",
	"M907 Z1300",
	"M150 T0 P3",
	"M907 E1250",
	"M92 X144",
	"M92 Y144",
	"M92 Z400",
	"M92 E369",
	"M12004 X0",
	"M12004 Y1",
	"M12004 Z0",
	"M12004 T0 E1",
	"M12004 T1 E0",
	"M290 P500.000000",
	"M149 T1",
	"M290 I50.000000",
	"M106 S0",
	"M104 T0 S0",
	"M301 T0 F0.57",
	"M301 T0 P9",
	"M150 T1 P0",
	"M301 T0 I0.2",
	"M301 T0 i100",
	"M301 T0 D40",
	"M292 T0 R23.000000",
	"M301 T0 C0.06",
	"M150 T1 P1",
	"M301 T0 E0",
	"M301 T0 R25",
	"M292 T0 P75.000000",
	"M292 T0 V24.000000",
	"M292 T0 R23.000000",
	"M150 T1 P2",
	"M12005 T0 F1.000000",
	"M12005 T0 O0.000000",
	"M104 T1 S0",
	"M301 T1 F0.57",
	"M301 T1 P9",
	"M150 T1 P3",
	"M301 T1 I0.2",
	"M301 T1 i100",
	"M301 T1 D40",
	"M301 T1 C0.06",
	"M301 T1 E0",
	"M301 T1 R25",
	"M292 T1 P75.000000",
	"M292 T1 V24.000000",
	"M292 T1 R0.000000",
	"M12005 T1 F0.985000",
	"M12005 T1 O0.750000",
	"M140 S0",
	"M304 F2.5",
	"M304 P125",
	"M304 I0",
	"M304 i60",
	"M304 D0",
	"M304 C0",
	"M292 T1 R23.000000",
	"M304 R25",
	"M291 R1.800000",
	"M291 A0.000500",
	"M291 V24.000000",
	"M12006 F0.924000",
	"M12006 O1.840000",
	"M143 T0 P0 r13 g56 b70",
	"M143 T1 P0 r13 g56 b70",
	"M218 T0 X0.000000 Y0.000000",
	"M218 T1 X0 Y0",
	"M145 T0",
	"M104 T0 S205",
	"M140 S60",
	"G28 Z0",
	"G28 X0 Y0",
	"G1 X328.0 Y185.0 F9000.0",
	"G1 X338.0 Y185.0 F1000.0",
	"G1 X338.0 Y209.0 F1000.0",
	"G1 X328.0 Y209.0 F9000.0",
	"T1 F5000",
	"M218 T0 Z0",
	"M218 T1 Z0",
	"M405 S1",
	"G1 X155.000000 Y20.000000 F9000.000000"
};


const char  term_s5_gcode[][50] = {
	"M104 T0 S0",
	"M140 S0",
	"G28 Z0",
	"G28 X0 Y0"
};


unsigned int  init_s5_gcode_lines = sizeof(init_s5_gcode) / 50;
unsigned int  term_s5_gcode_lines = sizeof(term_s5_gcode) / 50;

