#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <limits.h>
#include <linux/serial.h>
#include <poll.h>
#include <errno.h>
#include <stropts.h>
#include <asm/ioctls.h>
#include <libgen.h>
#include <sys/timerfd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <setjmp.h>
#include <execinfo.h>
#include <stddef.h>

#include "linereader.h"
#include "init_gcode.h"
#include "packetizer.h"


/*****************************************************************************/
/*** Macros								   ***/
/*****************************************************************************/
/* TCP port to send packets to */
#define SERVER_TCP_PORT		1979
#define MAX_GCODE_LEN		100
#define HEIGHT_ADJUSTMENT	6.415

/* Macro to calculate the number of members in an array */
#define ARRAY_SIZE(arr)		(sizeof(arr) / sizeof((arr)[0]))

#define DEBUG


/*****************************************************************************/
/*** Types								   ***/
/*****************************************************************************/
struct tcp_command_t
{
	unsigned char	type;
	unsigned char	cmd[UCHAR_MAX - sizeof(unsigned char)];
} __attribute__((packed));

struct packet_motion_command_t {
	unsigned char	data[sizeof(((struct tcp_command_t *)0)->cmd)];
} __attribute__((packed));

struct packet_command_t {
	uint32_t	refnr;
	unsigned char	data[sizeof(((struct tcp_command_t *)0)->cmd) - sizeof(unsigned char)];
} __attribute__((packed));

struct packet_reply_t {
	uint32_t	refnr;
	unsigned char	data[sizeof(((struct tcp_command_t *)0)->cmd) - sizeof(unsigned char)];
} __attribute__((packed));

struct packet_queue_request_t {
	uint32_t	refnr;
} __attribute__((packed));

struct packet_queue_reply_t {
	uint32_t	refnr;
	unsigned char	inflight_size;
	unsigned char	inflight_used;
	unsigned char	motion_size;
	unsigned char	motion_used;
} __attribute__((packed));


/*****************************************************************************/
/*** Globals								   ***/
/*****************************************************************************/
/* Command line options */
static char			*filename;                /* Name of the file to be printed */
static int			verbosity = 0;            /* Verbosity level */

/* Back-trace data */
static void			*bt_trace[20];            /* Back-trace buffer */
static size_t			bt_count;                 /* Back-trace count */
static jmp_buf			bt_env;                   /* Back-trace environment */

/* Server address */
static struct hostent		*server;                  /* Server IP address */
static short			port;                     /* Server TCP port number */

static struct pollfd		fds[1];                   /* File descriptors to poll in the main loop */

/* File read data */
static void			*linereader;              /* File line reader instance */

static void			*packetizer;

/* Client socket data */
static int			sock;                     /* Network socket to server */

static int			send_state = 0;           /* State machine state to send init g-code, wait for temperatures and send print g-code */
static int			line_nr = 0;              /* Init/term g-code line number to send next */


/*****************************************************************************/
/*** Static functions							   ***/
/*****************************************************************************/
static void adjust_height(char *gcode, size_t size, float adjustment)
{
	if (!strncmp(gcode, "G0 ", 3) ||
	    !strncmp(gcode, "G1 ", 3)) {
		unsigned int	start_index = 0;
		float		z_value;
		char		*endptr;
		char		trail[size];

		/* Find where the Z parameter starts */
		while (gcode[start_index]) {
			if (gcode[start_index] == 'Z')
				break;
			start_index++;
		}

		/* Check if a Z parameter was found */
		if (!gcode[start_index])
			return;

		/* Get the Z value */
		z_value = strtof(&gcode[start_index + 1], &endptr);

		/* Make a copy of the trailing data */
		strncpy(trail, endptr, sizeof(trail));

		/* Attach the adjusted Z value and the trailing data */
		sprintf(&gcode[start_index + 1], "%.3f%s", z_value + adjustment, trail);
	}
}


#ifdef DEBUG
static unsigned long timestamp_ms(void)
{
	struct timespec		timestamp;
	unsigned long		timestamp_us;

	clock_gettime(CLOCK_MONOTONIC, &timestamp);

	timestamp_us = (timestamp.tv_sec)  * 1000000 +
	               (timestamp.tv_nsec) / 1000;

	return timestamp_us / 1000;
}
#endif /* DEBUG */


static int send_marlin_command(char *data)
{
	static uint32_t		refnr = 0;
	size_t			len = strlen(data);
	struct tcp_command_t	tcp_command = {
		.type = 'C'
	};
	struct packet_command_t	*packet_command = (struct packet_command_t *)&tcp_command.cmd;

	if (len > sizeof(tcp_command.cmd)) {
		fprintf(stderr, "Error sending %u byte marlin command (%u max.)\n", (unsigned int)len, (unsigned int)sizeof(tcp_command.cmd));
		return -1;
	}

	memcpy(&packet_command->refnr, &refnr, sizeof(packet_command->refnr));
	memcpy(packet_command->data, data, len);

	refnr++;

	return packetizer_send(packetizer, &tcp_command, sizeof(tcp_command.type) + sizeof(packet_command->refnr) + len);
}


static int send_marlin_motion_command(char *data)
{
	size_t			len = strlen(data);
	struct tcp_command_t	tcp_command = {
		.type = 'M'
	};

	if (len > sizeof(tcp_command.cmd)) {
		fprintf(stderr, "Error sending %u byte marlin motion command (%u max.)\n", (unsigned int)len, (unsigned int)sizeof(tcp_command.cmd));
		return -1;
	}

	memcpy(tcp_command.cmd, data, len);

	return packetizer_send(packetizer, &tcp_command, sizeof(tcp_command.type) + len);
}


static int send_queue_command(void)
{
	static uint32_t		refnr = 0;
	struct tcp_command_t	tcp_command = {
		.type = 'Q'
	};
	struct packet_queue_request_t	*packet_queue_request = (struct packet_queue_request_t *)&tcp_command.cmd;

	fprintf(stderr, "Sending queue command refnr. 0x%.8x'\n", refnr);

	memcpy(&packet_queue_request->refnr, &refnr, sizeof(packet_queue_request->refnr));

	refnr++;

	return packetizer_send(packetizer, &tcp_command, sizeof(tcp_command.type) + sizeof(struct packet_queue_request_t));
}


static void request_temperature(void)
{
	char	gcode_line[8] = "M105";

#ifdef DEBUG
	fprintf(stderr, "Requesting temperatures\n");
#endif /* DEBUG */

	send_marlin_command(gcode_line);
}


static void handle_temperature_reply(char *data, size_t length)
{
	float	t0_temperature;
	float	t0_setpoint;
	float	t1_temperature;
	float	t1_setpoint;
	float	b_temperature;
	float	b_setpoint;
	char	*endptr;

	/* Example: " T0:49.2/205.0@255p1e1f65535/0 T1:31.7/0.0@0p1e0f65535/0 B49.3/60.0@255" */
	while (*data && *data != 'T')
		data++;

	if (!strncmp(data, "T0:", 3)) {
		t0_temperature = strtof(&data[3], &endptr);
		if (*endptr != '/')
			return;

		t0_setpoint = strtof(&endptr[1], &endptr);
		if (*endptr != '@')
			return;
		data++;
	}

	while (*data && *data != 'T')
		data++;

	if (!strncmp(data, "T1:", 3)) {
		t1_temperature = strtof(&data[3], &endptr);
		if (*endptr != '/')
			return;

		t1_setpoint = strtof(&endptr[1], &endptr);
		if (*endptr != '@')
			return;
		data++;
	}

	while (*data && *data != 'B')
		data++;

	b_temperature = strtof(&data[1], &endptr);
	if (*endptr != '/')
		return;

	b_setpoint = strtof(&endptr[1], &endptr);
	if (*endptr != '@')
		return;

	fprintf(stderr, "Hot-end 0: %.1f/%.1f, hot-end 1: %.1f/%.1f, bed: %.1f/%.1f\n", t0_temperature, t0_setpoint, t1_temperature, t1_setpoint, b_temperature, b_setpoint);

	if ((t0_temperature >= t0_setpoint) &&
	    (t1_temperature >= t1_setpoint) &&
	    (b_temperature >= b_setpoint)) {
		fprintf(stderr, "Temperature setpoints reached\n");
		/* Proceed to the next state to start sending print g-code */
		if (send_state == 1)
			send_state++;
		/* Start calling the write_data_to_server function upon output buffer space availability again */
		fds[0].events |= POLLOUT;
		return;
	}

	usleep(200000);
	request_temperature();
}


static void handle_marlin_broadcast(void *data, unsigned char len)
{
#ifdef DEBUG
	fprintf(stderr, "Received broadcast '%.*s'\n", len, (char *)data);
#endif /* DEBUG */
}


static void handle_marlin_reply(void *data, unsigned char len)
{
	uint32_t		refnr = 0;
	struct packet_reply_t	*packet_reply = (struct packet_reply_t *)data;

	memcpy(&refnr, &packet_reply->refnr, sizeof(refnr));

#ifdef DEBUG
	fprintf(stderr, "Received reply with refnr. 0x%.8x '%.*s'\n", refnr, len - (int)sizeof(packet_reply->refnr), (char *)&packet_reply->data);
#endif /* DEBUG */

	handle_temperature_reply((char *)&packet_reply->data, len - sizeof(packet_reply->refnr));
}


static void handle_queue_reply(void *data, unsigned char len)
{
	struct packet_queue_reply_t	*packet_queue_reply = (struct packet_queue_reply_t *)data;
	uint32_t			refnr;

	/* Memcpy: Source may be unaligned */
	memcpy(&refnr, &packet_queue_reply->refnr, sizeof(refnr));

	fprintf(stderr, "Received queue reply: refnr = 0x%.8x, inflight_size = %d, inflight_used = %d, motion_size = %d, motion_used = %d\n",
	                refnr, packet_queue_reply->inflight_size, packet_queue_reply->inflight_used, packet_queue_reply->motion_size, packet_queue_reply->motion_used);

	if (!packet_queue_reply->motion_used && send_state == 4) {
		/* Proceed to the next state to end */
		send_state++;
		return;
	}

	usleep(200000);
	send_queue_command();
}


static void handle_tcp_packet(void *user_data, void *packet, unsigned char packet_len)
{
	/* Command handler table */
	static struct handler_table_t {
		unsigned char		type;
		void			(*handler)(void *data, unsigned char len);
	} handler_table[] = {
		{.type = 'B', .handler = handle_marlin_broadcast},
		{.type = 'R', .handler = handle_marlin_reply},
		{.type = 'Q', .handler = handle_queue_reply}
	};

	struct tcp_command_t	*tcp_command = (struct tcp_command_t *)packet;
	int			table_ndx = 0;

	while (table_ndx < ARRAY_SIZE(handler_table)) {
		if (tcp_command->type == handler_table[table_ndx].type && handler_table[table_ndx].handler) {
			handler_table[table_ndx].handler(tcp_command->cmd, packet_len - offsetof(struct tcp_command_t, cmd));
			return;
		}
		table_ndx++;
	}
	fprintf(stderr, "Error handling unsupported %.2x packet (%d bytes)\n", tcp_command->type, packet_len - (int)sizeof(tcp_command->type));
}


static ssize_t write_tcp_packet(void *user_data, void *packet, ssize_t packet_len)
{
	return write(sock, packet, packet_len);
}


static ssize_t read_data_from_server(void)
{
	ssize_t		bytes;
	unsigned char	buffer[512];

	bytes = read(sock, buffer, sizeof(buffer));
	if (bytes > 0)
		packetizer_process(packetizer, buffer, bytes);
	else if (!bytes) {
		fprintf(stderr, "Connection closed\n");
		exit(1);
	} else {
		perror("Error reading network socket");
		exit(1);
	}
	return bytes;
}

static void write_data_to_server(void)
{
	char			gcode_line[MAX_GCODE_LEN];

	switch (send_state) {
	case 0:
		if (line_nr >= init_s5_gcode_lines) {
			line_nr = 0;
			send_state++;
			break;
		}

		strncpy(gcode_line, init_s5_gcode[line_nr++], sizeof(gcode_line));
		adjust_height(gcode_line, sizeof(gcode_line), HEIGHT_ADJUSTMENT);

#ifdef DEBUG
		fprintf(stderr, "%ld: Sending init line %d/%d '%s'\n", timestamp_ms(), line_nr, init_s5_gcode_lines, gcode_line);
#else
		if (gcode_line[0] != 'G')
			fprintf(stderr, "Sending '%s'\n", gcode_line);
#endif /* DEBUG */

		if (gcode_line[0] == 'G')
			send_marlin_motion_command(gcode_line);
		else
			send_marlin_command(gcode_line);
		break;

	case 1:
		/* Request temperatures from the printer */
		request_temperature();
		/* Stop calling the function upon output buffer space availability */
		fds[0].events &= ~POLLOUT;
		break;

	case 2:
		/* Read the next g-code line from the file */
		if (linereader_read(linereader, gcode_line, sizeof(gcode_line)) < 0) {
			send_state++;
			break;
		}

		/* Skip comment lines */
		if (!strlen(gcode_line) ||
		    gcode_line[0] == ';')
			break;

		adjust_height(gcode_line, sizeof(gcode_line), HEIGHT_ADJUSTMENT);

#ifdef DEBUG
		fprintf(stderr, "%ld: Sending file line '%s'\n", timestamp_ms(), gcode_line);
#else
		if (gcode_line[0] != 'G')
			fprintf(stderr, "Sending '%s'\n", gcode_line);
#endif /* DEBUG */

		if (gcode_line[0] == 'G')
			send_marlin_motion_command(gcode_line);
		else
			send_marlin_command(gcode_line);
		break;

	case 3:
		if (line_nr >= term_s5_gcode_lines) {
			line_nr = 0;
			send_state++;
			send_queue_command();
			break;
		}

		strncpy(gcode_line, term_s5_gcode[line_nr++], sizeof(gcode_line));
		adjust_height(gcode_line, sizeof(gcode_line), HEIGHT_ADJUSTMENT);

#ifdef DEBUG
		fprintf(stderr, "%ld: Sending term line %d/%d '%s'\n", timestamp_ms(), line_nr, term_s5_gcode_lines, gcode_line);
#else
		if (gcode_line[0] != 'G')
			fprintf(stderr, "Sending '%s'\n", gcode_line);
#endif /* DEBUG */

		if (gcode_line[0] == 'G')
			send_marlin_motion_command(gcode_line);
		else
			send_marlin_command(gcode_line);
		break;

	case 4:
		break;

	default:
		fprintf(stderr, "Printing completed, exitting\n");
		exit(0);
		break;
	}
}


/*****************************************************************************/
/*** Utilitarian static functions					   ***/
/*****************************************************************************/
static int init(void)
{
	struct sockaddr_in	server_addr;
	int			result = 0;

	/* Create a packet line-reader instance */
	if ((result = linereader_new(&linereader, filename)) < 0) {
		perror("Error creating linereader instance");
		goto err_linereader;
	}

	/* Create a TCP socket */
	if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		perror("Error opening socket");
		result = -errno;
		goto err_socket;
	}

	/* Configure the socket */
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port   = htons(port);
	memcpy(&server_addr.sin_addr.s_addr, server->h_addr, server->h_length);

	/* Create a packetizer */
	if ((result = packetizer_new(&packetizer, NULL, handle_tcp_packet, NULL, write_tcp_packet)) < 0) {
		fprintf(stderr, "Error %d creating packetizer\n", result);
		goto err_packetizer;
	}

	/* Connect to server */
	if ((result = connect(sock, (struct sockaddr *)&server_addr, sizeof(server_addr))) < 0) {
		perror("Error connecting");
		result = -errno;
		goto err_connect;
	}

	return 0;

err_connect:
	packetizer_delete(&packetizer);
err_packetizer:
	close(sock);
err_socket:
	linereader_delete(&linereader);
err_linereader:
	return result;
}


static void term(void)
{
	close(sock);
	linereader_delete(&linereader);
}


static void exit_handler(int ev, void *arg)
{
	term();
}


/*
 * This function installed as a signal handler, and is called automatically
 * whenever a SIGINT, SIGQUIT, SIGTERM or SIGSEGV is received. Its purpose is
 * to exit normally, enabling the terminator to shut down all subsystems and to
 * pull Marlin into reset.
 * Handling of SIGSEGV is a special one. Because printing is not recommended
 * within a signal handler, the back-trace is stored and a long jump to main
 * is made, so it can be printed in main context.
 */
static void signal_handler(int signum)
{
	switch (signum) {
	case SIGINT:
	case SIGQUIT:
	case SIGTERM:
		/* Exit gracefully */
		exit(signum);
		break;
	case SIGSEGV:
		/* Save the trace in statics */
		bt_count = backtrace(bt_trace, ARRAY_SIZE(bt_trace));

		/* Jump to main context */
		longjmp(bt_env, signum);
	}
}


/*****************************************************************************/
/*** Functions								   ***/
/*****************************************************************************/
int main(int argc, char* argv[])
{
	int			arg;
	int			signum;
	int			result = 0;

	/* Install signal handler */
	signal(SIGINT,  signal_handler);
	signal(SIGQUIT, signal_handler);
	signal(SIGTERM, signal_handler);
	signal(SIGSEGV, signal_handler);
	signal(SIGPIPE, SIG_IGN); /* Ignore SIGPIPE */

	/* Create an environment copy to allow jumping here from signal handler, decoupling calls to syslog, sync et all */
	if ((signum = setjmp(bt_env)) != 0) {
		char		**strings = backtrace_symbols(bt_trace, bt_count);
		size_t		index;

		fprintf(stderr, "Caught signal %d\n", signum);
		for (index = 0; index < bt_count; index++)
			fprintf(stderr, "Back-trace: %s\n", strings[index]);
		exit(EXIT_FAILURE);
	}

	/* Initialize non-zero defaults */
	filename = "50mms_cv_60-190segss_0.25inc.gcode";
	server   = gethostbyname("localhost");
	port     = SERVER_TCP_PORT;

	/* Process the command line arguments */
	while ((arg = getopt(argc, argv, "g:p:s:vh")) != -1) {
		switch (arg) {
		case 'g':
			/* Retrieve G-code file name */
			filename = optarg;
			break;
		case 'p':
			/* Retrieve server TCP port */
			port = strtol(optarg, NULL, 10);
			break;
		case 's':
			/* Retrieve server IP address */
			server = gethostbyname(optarg);
			break;
		case 'v':
			/* Be verbose */
			verbosity++;
			break;
		default:
			result = -1;
		case 'h':
			fprintf(stderr, "Usage: %s\n", basename(argv[0]));
			fprintf(stderr, "-v                 Be verbose\n");
			fprintf(stderr, "-h                 Show this help\n");
			return result;
		}
	}
	argc -= optind;
	argv += optind;

	if (argc) {
		fprintf(stderr, "Error: Positional arguments are not supported\n");
		exit(-1);
	}

	if (init() < 0)
		exit(-1);
	on_exit(exit_handler, NULL);

	memset(&fds, 0, sizeof(fds));
	fds[0].fd      = sock;
	fds[0].events |= POLLIN;
	fds[0].events |= POLLOUT;

	for (;;) {
		if (poll(fds, ARRAY_SIZE(fds), -1) == -1) {
			perror("Error polling socket");
			result = -errno;
			break;
		}

		if (fds[0].revents & POLLIN)
			read_data_from_server();

		if (fds[0].revents & POLLOUT)
			write_data_to_server();
	}

	return result;
}
