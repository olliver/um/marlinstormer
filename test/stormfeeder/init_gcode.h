#ifndef INIT_S5_GCODE
#define INIT_S5_GCODE


extern const char          init_s5_gcode[][50];
extern const unsigned int  init_s5_gcode_lines;

extern const char          term_s5_gcode[][50];
extern const unsigned int  term_s5_gcode_lines;


#endif /* INIT_S5_GCODE */

