MarlinStormer
=============
MarlinStormer is a test program to send g-code commands to the Marlin motion controller at the highest rate Marlin allows. This program enables the user to find Marlin's limits and determine regression of Marlin's limits after modifications.

Building and installing
-----------------------
Build and install MarlinStormer with:<br>
`$ ./build.sh`<br>
_Note: Update the script with the IP address of YOUR printer, or you'll be screwing up mine._

Log onto your printer using SSH:<br>
`$ ssh -lroot 10.183.0.170`

Kill Griffin:<br>
`# systemctl stop griffin.*`

Run Marlinstormer:<br>
`# ./marlinstormer`

TCP communication
-----------------
TCP port | 1979<br>
Timeout | 30 seconds<br>
Maximum sessions at a time | 8

Packet layout
-------------
All packets have the following layout:<br>
```
[0] Start of Header (ASCII SOH, value 0x01)
[1] Payload size (value 0x00...0xff)
[2..257] Payload data
```
This is defined as follows:
```C
struct packet_t
{
	unsigned char	soh;
	unsigned char	payload_size;
	unsigned char	payload[UCHAR_MAX];
} __attribute__((packed));
```
_Note1: Both received and transmitted packets must conform to this structure._<br>
_Note2: This limits data lengths to 255 bytes. Some marlin out-of-band blocks are longer!_<br>

Payload contents
----------------
First byte of the payload always designates the function of the packet, the
packet type, and the structure of the optional bytes following (if any).

This is defined as follows:
```C
struct envelope_t
{
	unsigned char	type;
	unsigned char	packet[UCHAR_MAX - sizeof(unsigned char /* type */)];
} __attribute__((packed));
```
_Note: A specific 'type' designates a specific data structure in 'packet', but
this does not automatically imply a fixed length. Because the payload size is
available in the underlying layer, the structure can end with a variable sized
member. Hence the packet size may differ between individual instances of the
same type._<br>

Client packet type 'M'
----------------------
The client 'M'-type packet instructs MarlinStormer to send a motion g-code
command to Marlin, taking both the number of Marlin commands in-flight and the
number of free motion planner buffer positions into account. This packet does
not trigger a reply from MarlinStormer.

The packet structure is basically an unterminated string containing the
actual g-code command, and it is defined as follows:
```C
struct packet_motion_command_t {
	unsigned char	data[sizeof(((struct envelope_t *)0)->packet)];
} __attribute__((packed));
```
Example of a complete packet, including soh, length and type:
```
data @0xa650047c, 35 bytes:
data[0x0000]:  01 21 4d 47 31 20 58 33 32 36 2e 34 34 33 30 34  .!MG1 X326.44304
data[0x0010]:  20 59 32 32 39 2e 37 37 37 37 38 20 46 39 30 30   Y229.77778 F900
data[0x0020]:  30 2e 30                                         0.0
```

Client packet type 'C'
----------------------
The client 'C'-type packet instructs MarlinStormer to send a non-motion g-code
command to Marlin, taking only the number of Marlin commands in-flight into
account.
This packet triggers a reply from MarlinStormer, if it has received a reply from
Marlin.
The reference number is an arbitrary number picked by the client. It is not
checked by MarlinStormer in any way. MarlinStormer copies the reference number
given with this packet into the packet containing the matching reply.

The packet structure is a 32-bit reference number, followed by an unterminated
string containing the actual g-code command, and it is defined as follows:
```C
struct packet_command_t {
	uint32_t	refnr;
	unsigned char	data[sizeof(((struct envelope_t *)0)->packet) - sizeof(uint32_t /* refnr */)];
} __attribute__((packed));
```
Example of a complete packet, including soh, length and type:
```
data @0xa650047c, 23 bytes:
data[0x0000]:  01 15 43 58 00 00 00 4d 31 32 30 30 36 20 46 30  ..CX...M12006 F0
data[0x0010]:  2e 39 32 34 30 30 30                             .924000
```

MarlinStormer packet type 'R'
-----------------------------
The MarlinStormer 'R'-type packet carries Marlin's reply to a non-motion g-code
command sent earlier.
The reference number matches the reference number of the 'C'-type packet this
reply belongs to.

The packet structure is a 32-bit reference number, followed by an unterminated
string containing the actual Marlin reply, and it is defined as follows:
```C
struct packet_reply_t {
	uint32_t	refnr;
	unsigned char	data[sizeof(((struct envelope_t *)0)->packet) - sizeof(uint32_t /* refnr */)];
} __attribute__((packed));
```
Example of a complete packet, including soh, length and type:
```
data @0xaefe9668, 78 bytes:
data[0x0000]:  01 4c 52 f0 01 00 00 20 54 30 3a 33 34 2e 36 2f  .LR.... T0:34.6/
data[0x0010]:  32 30 35 2e 30 40 32 35 35 70 31 65 31 66 36 35  205.0@255p1e1f65
data[0x0020]:  35 33 35 2f 30 20 54 31 3a 32 39 2e 39 2f 30 2e  535/0 T1:29.9/0.
data[0x0030]:  30 40 30 70 31 65 30 66 36 35 35 33 35 2f 30 20  0@0p1e0f65535/0 
data[0x0040]:  42 33 33 2e 33 2f 36 30 2e 30 40 32 35 35        B33.3/60.0@255
```

Client packet type 'Q'
----------------------
The client 'Q'-type packet instructs MarlinStormer to reply it's internal
transport layer queue status.
The reference number is an arbitrary number picked by the client. It is not
checked by MarlinStormer in any way. MarlinStormer copies the reference number
given with this packet into it's reply.

The packet structure is just a 32-bit reference number, and it is defined as
follows:
```C
struct packet_queue_request_t {
	uint32_t	refnr;
} __attribute__((packed));
```
Example of a complete packet, including soh, length and type:
```
data @0xa640047c, 7 bytes:
data[0x0000]:  01 05 51 42 04 00 00                             ..QB...
```

MarlinStormer packet type 'Q'
-----------------------------
The MarlinStormer 'Q'-type packet carries the reply to the queue status request,
a queue status report.
The reference number matches the reference number of the 'Q'-type packet this
reply belongs to.

The packet structure is defined as follows:
```C
struct packet_queue_reply_t {
	uint32_t	refnr;
	unsigned char	inflight_size;
	unsigned char	inflight_used;
	unsigned char	motion_size;
	unsigned char	motion_used;
} __attribute__((packed));
```
Example of a complete packet, including soh, length and type:
```
data @0xa64feb40, 11 bytes:
data[0x0000]:  01 09 51 e0 06 00 00 05 00 0f 00                 ..Q........
```

MarlinStormer packet type 'B'
-----------------------------
The MarlinStormer 'B'-type packet carries a slave (Marlin) initiated in-band
message.

The packet structure is basically an unterminated string containing the
actual Marlin message, and it is defined as follows:
```C
struct packet_reply_t {
	unsigned char	data[sizeof(((struct envelope_t *)0)->packet)];
} __attribute__((packed));
```
